package com.zlf.collection;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Project: bee-projects</p>
 * <p>Title: EduCourseNoteBean.java</p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2016 </p>
 * <p>Company: 华炜云商科技有限公司 www.hwtech.cc</p>
 *
 * @author Administrator
 * @version 1.0
 * @date 2017/2/17 17:40
 */
public class TestListSub {

    @Test
    public void test(){
        List<String> stringList=new ArrayList<>();
        stringList.add("saaa");
        stringList.add("vvvv");
        stringList.add("bbbb");
        System.out.println(stringList);
        stringList=stringList.subList(0,1);
        System.out.println(stringList);
    }
}

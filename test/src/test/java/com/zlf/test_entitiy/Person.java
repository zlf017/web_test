package com.zlf.test_entitiy;

import java.util.Date;

/**
 * Created by zlf on 17-2-18.
 */
public class Person {

    private Integer id;
    private String name;
    private Date birth;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }
}

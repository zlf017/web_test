package com.zlf.clazz;

import com.zlf.test_entitiy.Person;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Date;

/**
 * Created by zlf on 17-2-18.
 */
public class TestField {

    @Test
    public void test() throws IllegalAccessException {
        Person person=new Person();
        person.setId(1);
        person.setName("zhangli");
        person.setBirth(new Date());

        Field[] fields = person.getClass().getDeclaredFields();
        System.out.println(fields.length);


        for (Field field:fields){
            field.setAccessible(true);
            System.out.println(field.getName());
            System.out.println(field.get(person));
        }
    }
}

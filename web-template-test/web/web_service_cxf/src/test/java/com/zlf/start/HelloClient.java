package com.zlf.start;

import com.zlf.cxf.service.util.HelloWorld;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.frontend.ClientProxyFactoryBean;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Created by Administrator on 2016/12/22.
 *
 * @author zlf
 */
public class HelloClient {
    @Test
    public void test() {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.getInInterceptors().add(new LoggingInInterceptor());
        factory.getOutInterceptors().add(new LoggingOutInterceptor());
        factory.setServiceClass(HelloWorld.class);
        factory.setAddress("http://localhost:8080/spring_web_service_cxf/ws/soap/hello"); //地址为以上发布的地址
        HelloWorld client = (HelloWorld) factory.create();
        String reply = client.sayHi("HI");
        System.out.println("Server said: " + reply);
        System.exit(0);
    }

}

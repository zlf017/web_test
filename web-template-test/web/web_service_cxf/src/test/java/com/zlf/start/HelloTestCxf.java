package com.zlf.start;

import com.zlf.cxf.service.util.HelloWorld;
import com.zlf.cxf.service.util.HelloWorldImpl;
import com.zlf.cxf.service.util.interceptor.CheckUser;
import org.apache.cxf.aegis.databinding.AegisDatabinding;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.interceptor.Interceptor;
import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.EndpointImpl;
import org.apache.cxf.message.Message;
import org.junit.Test;

import javax.xml.ws.Endpoint;
import java.util.List;

/**
 * Created by Administrator on 2016/12/22.
 *
 * @author zlf
 */
public class HelloTestCxf {
    @Test
    public void test() throws Exception {
        System.out.println("Starting Server");
        HelloWorldImpl implementor = new HelloWorldImpl();
        String address = "http://localhost:8999/helloWorld"; //发布地址";
        Endpoint endpoint =Endpoint.publish(address, implementor);
        System.out.println(endpoint);

        EndpointImpl endpointImpl  = (EndpointImpl) endpoint;

        //服务端的日志入拦截器
        List inInterceptors = endpointImpl.getInInterceptors();
        inInterceptors.add(new LoggingInInterceptor());
        inInterceptors.add(new CheckUser());

        List outInterceptors = endpointImpl.getOutInterceptors();
        outInterceptors.add(new LoggingOutInterceptor());



        Thread.sleep(1000000);
    }

}

package com.zlf.cxf.service.util.interceptor;

import javax.xml.namespace.QName;

import org.apache.cxf.binding.soap.SoapMessage;
import org.apache.cxf.headers.Header;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.phase.AbstractPhaseInterceptor;
import org.apache.cxf.phase.Phase;
import org.w3c.dom.Element;
public class CheckUser extends AbstractPhaseInterceptor<SoapMessage> {

	public CheckUser() {
		super(Phase.PRE_PROTOCOL);
		System.out.println("check Object");
	}

	public void handleMessage(SoapMessage msg) throws Fault {
		System.out.println("check user==");
		Header header = msg.getHeader(new QName("atguigu"));//获取请求头的数据
		if(header!=null){
			Element element=(Element) header.getObject();
			String name=element.getElementsByTagName("name").item(0).getTextContent();//获取名字为name的标签的第一个的内容
			String passwd =element.getElementsByTagName("passwd").item(0).getTextContent();
			System.out.println(name);
			if("zlf".equals(name)&&"201314".equals(passwd)){
				System.out.println("pass user");
				return;
			}
			
		}
		throw new Fault(new RuntimeException("用户名密码验证失败。"));
		
	}


}

package com.zlf.cxf.service.entity;

import java.util.Date;
import java.util.List;

public class Student {
	private String name;
	private List<String> book;
	private Date date;
	
	
	
	@Override
	public String toString() {
		return "Student [name=" + name + ", book=" + book + ", date=" + date + "]";
	}
	public Student(String name, List<String> book, Date date) {
		super();
		this.name = name;
		this.book = book;
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public List<String> getBook() {
		return book;
	}
	public void setBook(List<String> book) {
		this.book = book;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	
}

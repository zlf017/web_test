package com.zlf.interaeptor;

import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.Properties;

/**
 * Created by Administrator on 2016/12/19.
 *
 * @author zlf
 */
public class ExceptionResolver extends AbstractHandlerExceptionResolver {
    private String pathPatterns;
    private Map<String, Integer> statusCodes;
    private Properties exceptionMappings;

    @Override
    protected ModelAndView doResolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {

        System.out.println("========================================");
        System.out.println(ex.getMessage());
        System.out.println("----------------------------------------");
        return null;
    }


    public String getPathPatterns() {
        return pathPatterns;
    }

    public void setPathPatterns(String pathPatterns) {
        this.pathPatterns = pathPatterns;
    }

    public Map<String, Integer> getStatusCodes() {
        return statusCodes;
    }

    public void setStatusCodes(Map<String, Integer> statusCodes) {
        this.statusCodes = statusCodes;
    }

    public Properties getExceptionMappings() {
        return exceptionMappings;
    }

    public void setExceptionMappings(Properties exceptionMappings) {
        this.exceptionMappings = exceptionMappings;
    }
}

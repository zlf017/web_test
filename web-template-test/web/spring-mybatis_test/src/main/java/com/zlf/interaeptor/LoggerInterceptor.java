package com.zlf.interaeptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>Project: projects</p>
 * <p>Title: LoggerInterceptor.java</p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2015 </p>
 * <p>Company: 华炜云商科技有限公司 www.hwtech.cc</p>
 *
 * @author zh
 * @version 1.0
 * @date 2015-11-30
 */
public class LoggerInterceptor  implements HandlerInterceptor {

    //@Autowired
    //private SysLoggerService sysLoggerService;

    protected final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(getClass());

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object o, Exception e) throws Exception {

        try {
            if(e != null) {
                //有异常，说明没操作成功，不记录日志
                return;
            }
            String uri = request.getServletPath();
            String method=request.getMethod();

            logger.info("------------------");
            logger.debug(uri);
            logger.info(method);
            logger.info(request.getRemoteAddr());
            logger.info(request.getRequestURI());
            logger.info(request.getLocalAddr());
            logger.info(request.getRemoteAddr());
            logger.info("-------------------");
        }catch(Exception ie) {
            //日志记录不影响业务，即使日志记录出错，业务也要继续完成
            ie.printStackTrace();
            logger.error(ie);
        }
    }






}
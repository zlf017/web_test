package com.zlf.service.impl;

import com.zlf.conf.XMLMapperLoader;
import com.zlf.entity.GradeBean;
import com.zlf.mapper.GradeBeanMapper;
import com.zlf.service.DemoServcie;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Administrator on 2016/12/19.
 *
 * @author zlf
 */
@Service
public class DemoServiceImpl implements DemoServcie {
    @Autowired
    private GradeBeanMapper gradeBeanMapper;
    @Override
    public List<GradeBean> finaGradeBeanBy(GradeBean gradeBean) {

        return gradeBeanMapper.findGradeBeanByStatus(gradeBean);
    }
}

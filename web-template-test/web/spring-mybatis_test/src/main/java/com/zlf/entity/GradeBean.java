package com.zlf.entity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.zlf.util.CustomDateSerializer;
import com.zlf.util.ObjectSerialier;

import java.util.Date;

/**
 * Created by Administrator on 2016/12/19.
 *
 * @author zlf
 */
@JsonSerialize(using = ObjectSerialier.class)
public class GradeBean {

    private String gId;
    private String name;
    private int status;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date startDt;
    @JsonSerialize(using = CustomDateSerializer.class)
    private Date endDt;
    private String creator;

    public String getgId() {
        return gId;
    }

    public void setgId(String gId) {
        this.gId = gId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Date getStartDt() {
        return startDt;
    }

    public void setStartDt(Date startDt) {
        this.startDt = startDt;
    }

    public Date getEndDt() {
        return endDt;
    }

    public void setEndDt(Date endDt) {
        this.endDt = endDt;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }
}

package com.zlf.controller;

import com.zlf.conf.MapperRefresh;
import com.zlf.entity.GradeBean;
import com.zlf.service.DemoServcie;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Locale;

/**
 * Created by Administrator on 2016/12/19.
 *
 * @author zlf
 */
@Controller
public class GradeController {
    @Autowired
    private DemoServcie demoServcie;


    @RequestMapping("demoSelect")
    @ResponseBody
    public Object selectGradeBean(GradeBean gradeBean, HttpServletRequest request) throws IOException, ServletException {
        String sessionId = request.getRequestedSessionId();
//        if(null==sessionId||"null".equals(sessionId)){
//            return "";
//        }
        Enumeration<String> names = request.getHeaderNames();

        while (names.hasMoreElements()) {
            String hName = names.nextElement();
            System.out.println(hName + ":" + request.getHeader(hName));
        }
        System.out.println("----------------------------");
        System.out.println("QuueryString:" + request.getQueryString());
        System.out.println("authType:" + request.getAuthType());
        System.out.println("method:" + request.getMethod());
        System.out.println("pathInfo:" + request.getPathInfo());
        System.out.println("remoteUser:" + request.getRemoteUser());
        System.out.println("sessionId:" + request.getRequestedSessionId());
        System.out.println("requestURI:" + request.getRequestURI());
        System.out.println("requestURL:" + request.getRequestURL());
//        System.out.println("User_Principal_name:"+request.getUserPrincipal().getName());
        System.out.println("PathTranslated:" + request.getPathTranslated());

        System.out.println("dispatherType_name:" + request.getDispatcherType().name());
//        System.out.println("asyncContext_Timeout:"+request.getAsyncContext());
        System.out.println("contentLength:" + request.getContentLength());
        System.out.println("contentPath:" + request.getContextPath());
        System.out.println("ContentType:" + request.getContentType());
        System.out.println("charactorEncoding:" + request.getCharacterEncoding());
        System.out.println();

        System.out.println("localName" + request.getLocalName());
        System.out.println("localAddr:" + request.getLocalAddr());
        System.out.println("localAddr:" + request.getLocalAddr());
        System.out.println("local_displayName:" + request.getLocale().getDisplayName());
        Enumeration<Locale> locales = request.getLocales();
        while (locales.hasMoreElements()) {
            Locale locale = locales.nextElement();
            System.out.println(locale.getDisplayName() +
                    "  -country:" + locale.getCountry() +
                    " -language:" + locale.getLanguage() +
                    " -script:" + locale.getScript() +
                    " -iso3Country:" + locale.getISO3Country() +
                    " -displayScript:" + locale.getDisplayScript() +
                    " -displayVariant:" + locale.getDisplayVariant() +
                    " -displayCountry:" + locale.getDisplayCountry() +
                    ""
            );
        }


        System.out.println("param---");
        Enumeration<String> paramNames = request.getParameterNames();
        while (paramNames.hasMoreElements()) {
            request.getParameter(paramNames.nextElement());
        }
        System.out.println("param  ===end");
        System.out.println("------parts start");
       /* Collection<Part> parts=request.getParts();
        for(Part p:parts){
            System.out.println(p.getName() + "--" + p.getContentType() + "--size" + p.getSize());
            Collection<String> hNames=p.getHeaderNames();
            for(String str:hNames){
                System.out.print(p.getHeader(str)+"__");
            }
        }
        System.out.println("======parts end");*/


        return demoServcie.finaGradeBeanBy(gradeBean);
    }

    @RequestMapping("login1")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        return modelAndView;
    }

    @RequestMapping("d")
    public ModelAndView demo() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("demo");
        modelAndView.addObject("user", "zzzzllfl");


        return modelAndView;
    }

    @Autowired
    private SqlSessionFactoryBean sqlSessionFactoryBean;

    @RequestMapping("getJson")
    @ResponseBody
    public Object getJson() throws Exception {
//        Resource[] resources = new ClassPathResource[]{new ClassPathResource("com/zlf/mapper/GradeBeanMapper.xml")};
//        MapperRefresh mapperRefresh = new MapperRefresh(resources, sqlSessionFactoryBean.getObject().getConfiguration());
//        new Thread(mapperRefresh).start();
        return demoServcie.finaGradeBeanBy(new GradeBean());
    }

}

package com.zlf.controller;

import com.zlf.annotation.RequestLimit;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Project: bee-projects</p>
 * <p>Title: EduCourseNoteBean.java</p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2016 </p>
 * <p>Company: 华炜云商科技有限公司 www.hwtech.cc</p>
 *
 * @author Administrator
 * @version 1.0
 * @date 2017/2/28 10:18
 */
@Controller
public class VisitController {


    @RequestLimit(count=3,time=1000)
    @RequestMapping("/test")
    @ResponseBody
    public Object doAddBean(){
        System.out.println("---------------------------------");
        Map<String,String> map = new HashMap<>();
        map.put("zha","ss");
        return map;
    }
}

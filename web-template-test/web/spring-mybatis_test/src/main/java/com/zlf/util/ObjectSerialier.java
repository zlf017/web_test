package com.zlf.util;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;




public class ObjectSerialier extends JsonSerializer<Object> {

    @Override
    public void serialize(Object value, JsonGenerator jgen,
        SerializerProvider provider) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        Map<String,Object> map=new HashMap<>();
        Field[] fields=value.getClass().getDeclaredFields();
        for(Field field:fields){
            field.setAccessible(true);
            String name=field.getName();
            try {
                Object val=field.get(value);
                map.put(name,val);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        mapper.writeValue(jgen, map);
        }

}
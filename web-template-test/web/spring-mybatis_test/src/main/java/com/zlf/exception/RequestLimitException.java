package com.zlf.exception;

/**
 * <p>Project: bee-projects</p>
 * <p>Title: EduCourseNoteBean.java</p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2016 </p>
 * <p>Company: 华炜云商科技有限公司 www.hwtech.cc</p>
 *
 * @author Administrator
 * @version 1.0
 * @date 2017/2/28 10:23
 */
public class RequestLimitException extends Exception {
    private static final long serialVersionUID = 1364225358754654702L;

    public RequestLimitException() {
        super("HTTP请求超出设定的限制");
    }

    public RequestLimitException(String message) {
        super(message);
    }

}
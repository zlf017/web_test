package com.zlf.mapper;

import com.zlf.entity.GradeBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Administrator on 2016/12/19.
 *
 * @author zlf
 */
public interface GradeBeanMapper {

    List<GradeBean> findGradeBeanByStatus(GradeBean gradeBean);


}

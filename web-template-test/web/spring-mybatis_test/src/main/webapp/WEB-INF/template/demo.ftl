<!DOCTYPE html>
<html>
<head>
    <title>Hello</title>
</head>

<body>
This is my HTML page. <br>
<h1>${user}</h1>

<h2>====================================================================</h2>
就相当于一个模版：     .ftl
可以对其中的值替换：
也相当于jsp标签，可以判断数据输出，便利数据
格式化、截取数据、分割数据等


文件内容例如：
--------------if语句的使用----------------
<#if score gte 60>
及格
<#elseif score gte 80&&score lte 90>
良好
<#else>
高材生
</#if>
--------------空值判断、默认值---------------
${name!"未定义"}
--------------判断值是否存在------------
<#if name??>
name存在
<#else>
name不存在
</#if>
------------使用list遍历数据--------------
<#list userList as user>
    <#if  user_has_next> 最后一组:${user.name}-${user.password}<#else>${user.name}-${user.password}</#if>
</#list>
-----------其他内建函数-----------------
（1）日期格式化
${time?string("yyyy-MM-dd")}
（2）截取字符串
${str?substring(0,2)}
（2）indexof的使用
${str?last_index_of(",")}
（2）split的使用
<#list "12,13,14,15"?split(",") as item>
${item}
</#list>



</body>
</html>
package com.zlf.web.service;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.jaxws.endpoint.dynamic.JaxWsDynamicClientFactory;

/**
 * Created by Administrator on 2016/12/22.
 *  动态代理客户端
 * @author zlf
 */
public class ClientDynamicProxy {

    public static void main(String[] args) {

        JaxWsDynamicClientFactory factory = JaxWsDynamicClientFactory.newInstance();
        Client client = factory.createClient("http://localhost:8080/spring_web_service_cxf/ws/soap/hello?wsdl");

        try {
            Object[] results = client.invoke("sayHi", "world");
            System.out.println(results[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

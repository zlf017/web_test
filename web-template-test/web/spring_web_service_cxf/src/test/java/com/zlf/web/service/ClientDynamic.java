package com.zlf.web.service;

import org.apache.cxf.endpoint.Client;
import org.apache.cxf.endpoint.dynamic.DynamicClientFactory;

/**
 * Created by Administrator on 2016/12/22.
 *通用动态代理客户端
 * @author zlf
 */
public class ClientDynamic {

    public static void main(String[] args) {

        DynamicClientFactory factory = DynamicClientFactory.newInstance();
        Client client = factory.createClient("http://localhost:8080/ws/soap/hello?wsdl");

        try {
            Object[] results = client.invoke("sayHi", "world");
            System.out.println(results[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

package com.zlf.web.service;

import org.springframework.stereotype.Component;
import javax.jws.WebService;

/**
 * Created by Administrator on 2016/12/22.
 *
 * @author zlf
 */

@WebService
@Component  //让spring扫描到该类
public class HelloWorldImpl implements HelloWorld {
    public String sayHi(String text) {
        System.out.println("sayHi called");
        return "Hello " + text;
    }
}

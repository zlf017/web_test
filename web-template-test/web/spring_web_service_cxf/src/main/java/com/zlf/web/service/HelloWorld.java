package com.zlf.web.service;

import javax.jws.WebParam;
import javax.jws.WebService;

/**
 * Created by Administrator on 2016/12/22.
 *
 * @author zlf
 */
@WebService
public interface HelloWorld {
    String sayHi(@WebParam(name="text") String text); //这里的text为输入参数
}

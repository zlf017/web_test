<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/12/22
  Time: 11:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>

    <style>
        #list {
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        .body {
            width: 100%;
            height: 20%;
            font-size: 100;
            border: 1px solid greenyellow;
        }

        /* 改变滚动条样式
        .iScrollHorizontalScrollbar，这个样式应用到横向滚动条的容器。这个元素实际上承载了滚动条指示器。
        .iScrollVerticalScrollbar，和上面的样式类似，只不过适用于纵向滚动条容器。
         .iScrollIndicator，真正的滚动条指示器。
        .iScrollBothScrollbars，这个样式将在双向滚动条显示的情况下被加载到容器元素上。通常情况下其中一个（横向或者纵向）是可见的

        Styled scrollbars */

        .iScrollHorizontalScrollbar {
            position: absolute;
            z-index: 9999;
            height: 16px;
            left: 2px;
            right: 2px;
            bottom: 2px;
            overflow: hidden;
        }

        .iScrollHorizontalScrollbar.iScrollBothScrollbars {
            right: 18px;
        }

        .iScrollVerticalScrollbar {
            position: absolute;
            z-index: 9999;
            width: 16px;
            bottom: 2px;
            top: 2px;
            right: 2px;
            overflow: hidden;
        }

        .iScrollVerticalScrollbar.iScrollBothScrollbars {
            bottom: 18px;
        }

        .iScrollIndicator {
            position: absolute;
            background: #cc3f6e;
            border-width: 1px;
            border-style: solid;
            border-color: #EB97B4 #7C2845 #7C2845 #EB97B4;
            border-radius: 8px;
        }

        .iScrollHorizontalScrollbar .iScrollIndicator {
            height: 100%;
            background: -moz-linear-gradient(left, #cc3f6e 0%, #93004e 100%);
            background: -webkit-linear-gradient(left, #cc3f6e 0%, #93004e 100%);
            background: -o-linear-gradient(left, #cc3f6e 0%, #93004e 100%);
            background: -ms-linear-gradient(left, #cc3f6e 0%, #93004e 100%);
            background: linear-gradient(to right, #cc3f6e 0%, #93004e 100%);
        }

        .iScrollVerticalScrollbar .iScrollIndicator {
            width: 100%;
            background: -moz-linear-gradient(top, #cc3f6e 0%, #93004e 100%);
            background: -webkit-linear-gradient(top, #cc3f6e 0%, #93004e 100%);
            background: -o-linear-gradient(top, #cc3f6e 0%, #93004e 100%);
            background: -ms-linear-gradient(top, #cc3f6e 0%, #93004e 100%);
            background: linear-gradient(to bottom, #cc3f6e 0%, #93004e 100%);
        }

        /* end */


    </style>
</head>
<body onload="loaded()">
<div id="list">

    <div id="listValue">

    </div>

</div>
</body>
<script src="js/jquery-1.11.1.js"></script>
<script src="js/iscroll/iscroll.js"></script>
<script src="js/iscroll/iscroll-infinite.js"></script>
<script src="js/iscroll/iscroll-probe.js"></script>
<script src="js/iscroll/iscroll-zoom.js"></script>
<script>
    var myScroll;
    var s = $("#listValue");

    function loaded() {
        addData();
        myScroll = new IScroll('#list', {
                    mouseWheel: true,
                    scrollbars: true,   //显示滚动条,默认不现实
                    fadeScrollbars: true,   //淡出淡入，默认值：false
                    interactiveScrollbars: true, //让滚动条能拖动，用户可以与之交互,默认值：false
                    shrinkScrollbars: true,   //待研究
                    scrollbars: 'custom',
                    zoom: true,     //true启用缩放功能。 默认false
                    keyBindings: {pageUp: 33, pageDown: 34, end: 35, home: 36, left: 37, up: 38, right: 39, down: 40}//按钮绑定
                }
        );//滑动内容必须是在#list下的div
        /*
         * myScroll.destroy();
         myScroll = null;
         * */

    }

    document.addEventListener('touchmove', function (e) {
        e.preventDefault();
    }, false);


    function addData() {
        for (var i = 0; i < 20; i++) {
            s.append("<div class='body'>==============" + i + "</div>")
        }
    }
</script>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"/>
    <script src="js/jquery-1.11.1.js"></script>
    <title>iScroll下拉刷新上滑加载</title>
    <style>
        body, ul, li {
            padding: 0;
            margin: 0;
            border: 0
        }

        body {
            font-size: 12px;
            font-family: microsoft yahei
        }

        .header {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 45px;
            line-height: 45px;
            font-size: 16px;
            text-align: center;
            background: #e6e6e6
        }

        .footer {
            position: absolute;
            bottom: 0;
            left: 0;
            width: 100%;
            height: 48px;
            line-height: 48px;
            font-size: 16px;
            text-align: center;
            background: #e6e6e6
        }

        #wrapper {
            position: absolute;
            top: 45px;
            bottom: 48px;
            left: 0;
            width: 100%
        }

        #scroller li {
            padding: 0 10px;
            height: 60px;
            line-height: 60px;
            background: #ecf6ff;
            margin-top: 10px
        }

        #pullDown, #pullUp {
            padding: 0 10px;
            height: 30px;
            line-height: 30px;
            color: #888;
            text-align: center
        }
    </style>

    <script>
        var myScroll, pullDownEl, pullDownOffset, pullUpEl, pullUpOffset, generatedCount = 0;
        /*
         hScroll               false 禁止横向滚动 true横向滚动 默认为true
         vScroll               false 精致垂直滚动 true垂直滚动 默认为true
         hScrollbar         false隐藏水平方向上的滚动条
         vScrollbar         false 隐藏垂直方向上的滚动条
         fixedScrollbar  在iOS系统上，当元素拖动超出了scroller的边界时，滚动条会收缩，设置为true可以禁止滚动条超出
         scroller的可见区域。默认在Android上为true， iOS上为false
         fadeScrollbar  false 指定在无渐隐效果时隐藏滚动条
         hideScrollbar  在没有用户交互时隐藏滚动条 默认为true
         bounce            启用或禁用边界的反弹，默认为true
         momentum     启用或禁用惯性，默认为true，此参数在你想要保存资源的时候非常有用
         lockDirection  false取消拖动方向的锁定， true拖动只能在一个方向上（up/down 或者left/right）


         通用方法：
         （1）refresh                          在DOM树发生变化时，应该调用此方法
         （2）scrollTo()                     滚动到某个位置
         eg: myscroll.scrollTo(0,10,200,true);
         （3）scrollToElement()      滚动到某个元素
         eg: myscroll.scrolToElement("li:nth-child(10)",100);
         （4）detroy()                       完全消除myscroll及其占用的内存空间
         eg: myscroll.destroy()
       */
        function loaded() {
            //动画部分
            pullDownEl = document.getElementById('pullDown');
            pullDownOffset = pullDownEl.offsetHeight;
            pullUpEl = document.getElementById('pullUp');
            pullUpOffset = pullUpEl.offsetHeight;
            myScroll = new iScroll('wrapper', {
                useTransition: true,
                topOffset: pullDownOffset,
                zoom: true,
                zoomMax: 4,
                onRefresh: function () {
                    if (pullDownEl.className.match('loading')) {
                        pullDownEl.className = '';
                        pullDownEl.querySelector('.pullDownLabel').innerHTML = '下拉刷新';
                    } else if (pullUpEl.className.match('loading')) {
                        pullUpEl.className = '';
                        pullUpEl.querySelector('.pullUpLabel').innerHTML = '上拉加载更多';
                    }
                },
                onScrollMove: function () {

                    if (this.y > 5 && !pullDownEl.className.match('flip')) {
                        pullDownEl.className = 'flip';
                        pullDownEl.querySelector('.pullDownLabel').innerHTML = '释放刷新';
                        this.minScrollY = 0;
                    } else if (this.y < 5 && pullDownEl.className.match('flip')) {
                        pullDownEl.className = '';
                        pullDownEl.querySelector('.pullDownLabel').innerHTML = 'Pull down to refresh...';
                        this.minScrollY = -pullDownOffset;
                    } else if (this.y < (this.maxScrollY - 5) && !pullUpEl.className.match('flip')) {
                        pullUpEl.className = 'flip';
                        pullUpEl.querySelector('.pullUpLabel').innerHTML = '释放刷新';
                        this.maxScrollY = this.maxScrollY;
                    } else if (this.y > (this.maxScrollY + 5) && pullUpEl.className.match('flip')) {
                        pullUpEl.className = '';
                        pullUpEl.querySelector('.pullUpLabel').innerHTML = 'Pull up to load more...';
                        this.maxScrollY = pullUpOffset;
                    }
                },
                onScrollEnd: function () {
                    if (pullDownEl.className.match('flip')) {
                        pullDownEl.className = 'loading';
                        pullDownEl.querySelector('.pullDownLabel').innerHTML = '加载中';
                        pullDownAction();   // Execute custom function (ajax call?)
                    } else if (pullUpEl.className.match('flip')) {
                        pullUpEl.className = 'loading';
                        pullUpEl.querySelector('.pullUpLabel').innerHTML = '加载中';
                        pullUpAction(); // Execute custom function (ajax call?)
                    }
                }
            });

            loadAction();
        }
        document.addEventListener('touchmove', function (e) {
            e.preventDefault();
        }, false);//阻止冒泡
        document.addEventListener('DOMContentLoaded', function () {
            setTimeout(loaded, 0);
        }, false);

        //初始状态，加载第一页数据
        function loadAction() {
            var el, li;
            el = document.getElementById('thelist');
            for (i = 0; i < 10; i++) {
                li = document.createElement('li');
                li.innerText = '初始数据--' + (++generatedCount);
                el.appendChild(li, el.childNodes[0]);
            }
            myScroll.refresh();
        }

        //删除数据重新加载数据
        function pullDownAction() {
            setTimeout(function () {
                //这里执行刷新操作

                myScroll.refresh();
            }, 700);
        }

        //加载下一页数据
        function pullUpAction() {
            setTimeout(function () {
                var el, li;
                el = document.getElementById('thelist');
                for (i = 0; i < 10; i++) {
                    li = document.createElement('li');
                    li.innerText = '上拉加载--' + (++generatedCount);
                    el.appendChild(li, el.childNodes[0]);
                }
                myScroll.refresh();
            }, 700);
        }
    </script>
</head>
<body>

<div class="header">header</div>
<div id="wrapper">
    <div id="scroller">
        <div id="pullDown">
            <span class="pullDownLabel">下拉刷新</span>
        </div>
        <ul id="thelist">
            <!--<li>原始数据</li>-->
        </ul>
        <div id="pullUp">
            <span class="pullUpLabel">上拉加载更多</span>
        </div>
    </div>
</div>
<div class="footer">footer</div>

<script type="text/javascript" src="js/iscroll.js"></script>

</body>
</html>
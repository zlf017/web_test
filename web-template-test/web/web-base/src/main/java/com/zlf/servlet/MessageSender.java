package com.zlf.servlet;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;

import javax.servlet.ServletResponse;

public class MessageSender extends Thread {

    private ServletResponse connection;

    public MessageSender(ServletResponse connection) {
        this.connection = connection;
    }

    public void run() {
        try {
            while (true) {
                if (connection == null) {
                    synchronized (this) {
                        wait();
                    }
                }

                OutputStream out = connection.getOutputStream();
                out.write(getString().getBytes());
                out.flush();
                connection.flushBuffer();

                System.out.print(getString());

                Thread.sleep(1000);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("deprecation")
    private String getString() {
        return Thread.currentThread()+" CurrentTime "+new Date().toLocaleString() + "\n";
    }

}

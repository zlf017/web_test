package com.hua.entitiy.test37;

import java.util.List;

/**
 * Created by Administrator on 2016/11/17.
 */
public class T_User {
    private String id;
    private String userName;
    private String userPwd;
    private String relUserPwd;
    private String gender;
    private String email;
    private List<String> likes;
    private String explain;

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        T_User tUser = (T_User) o;

        if (id != null ? !id.equals(tUser.id) : tUser.id != null) return false;
        if (userName != null ? !userName.equals(tUser.userName) : tUser.userName != null) return false;
        if (userPwd != null ? !userPwd.equals(tUser.userPwd) : tUser.userPwd != null) return false;
        if (relUserPwd != null ? !relUserPwd.equals(tUser.relUserPwd) : tUser.relUserPwd != null) return false;
        if (gender != null ? !gender.equals(tUser.gender) : tUser.gender != null) return false;
        if (email != null ? !email.equals(tUser.email) : tUser.email != null) return false;
        if (likes != null ? !likes.equals(tUser.likes) : tUser.likes != null) return false;
        return explain != null ? explain.equals(tUser.explain) : tUser.explain == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (userName != null ? userName.hashCode() : 0);
        result = 31 * result + (userPwd != null ? userPwd.hashCode() : 0);
        result = 31 * result + (relUserPwd != null ? relUserPwd.hashCode() : 0);
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (likes != null ? likes.hashCode() : 0);
        result = 31 * result + (explain != null ? explain.hashCode() : 0);
        return result;
    }

    public void setUserName(String userName) {

        this.userName = userName;
    }

    public void setUserPwd(String userPwd) {
        this.userPwd = userPwd;
    }

    public void setRelUserPwd(String relUserPwd) {
        this.relUserPwd = relUserPwd;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLikes(List<String> likes) {
        this.likes = likes;
    }

    public void setExplain(String explain) {
        this.explain = explain;
    }

    public String getUserName() {

        return userName;
    }

    public String getUserPwd() {
        return userPwd;
    }

    public String getRelUserPwd() {
        return relUserPwd;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public List<String> getLikes() {
        return likes;
    }

    public String getExplain() {
        return explain;
    }

    @Override
    public String toString() {
        return "T_User{" +
                "userName='" + userName + '\'' +
                ", userPwd='" + userPwd + '\'' +
                ", relUserPwd='" + relUserPwd + '\'' +
                ", gender=" + gender +
                ", email='" + email + '\'' +
                ", likes=" + likes +
                ", explain='" + explain + '\'' +
                '}';
    }
}

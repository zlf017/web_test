package com.hua.servlet.test37;

import com.hua.entitiy.test37.T_User;
import com.hua.service.test37.UserOpService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;

/**
 * Created by Administrator on 2016/11/17.
 */
public class UserOpServelt extends HttpServlet {
    private UserOpService us = new UserOpService();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String oldPwd = req.getParameter("oldUserPwd");
        String newUserPwd = req.getParameter("newUserPwd");
        String relUserPwd = req.getParameter("relnewUserPwd");
        String[] likes = req.getParameterValues("like");
        if (newUserPwd != null && newUserPwd.equals(relUserPwd) && likes != null) {
            T_User tUser = new T_User();
            tUser.setId(req.getParameter("id"));
            String gender = req.getParameter("gender");
            tUser.setUserPwd(oldPwd);
            tUser.setRelUserPwd(relUserPwd);
            tUser.setLikes(Arrays.asList(likes));
            tUser.setEmail(req.getParameter("email"));
            tUser.setExplain(req.getParameter("explain"));
            if (gender != null && gender.length() == 1) {

                tUser.setGender(gender);
                tUser.setUserName(req.getParameter("userName"));
                T_User rettUser = us.updateT_User(tUser, oldPwd, req);
                System.out.println("-------------------" + rettUser);
                if (rettUser != null) {
                    req.getSession().setAttribute("tUser", rettUser);
                    req.getSession().setAttribute("message", "资料修改完成");
                    resp.sendRedirect("test37/practise1_4.jsp");
                    return ;
                }
            }


        }
        req.getSession().setAttribute("message", "资料修改异常");
        resp.sendRedirect("test37/practise1_4.jsp");
    }
}

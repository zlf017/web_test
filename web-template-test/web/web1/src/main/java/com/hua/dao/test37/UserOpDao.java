package com.hua.dao.test37;

import com.hua.entitiy.test37.T_User;
import com.hua.util.JDBCConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

/**
 * Created by Administrator on 2016/11/17.
 */
public class UserOpDao {

        private JDBCConnection jdbc=new JDBCConnection();
        public T_User updateUser(T_User tUser){

            Connection conn=jdbc.getConnection();
            //删除喜好
            String deleteLike="delete from t_user_like where userid=?";
            try {
                PreparedStatement ps=conn.prepareStatement(deleteLike);
                ps.setString(1,tUser.getId());
                ps.execute();

                jdbc.close();
                conn=jdbc.getConnection();
                String updateUserSql="update t_user set userpwd=? ,gender=? where id=?";
                ps=conn.prepareStatement(updateUserSql);
                ps.setString(1,tUser.getRelUserPwd());
                ps.setString(2,String.valueOf(tUser.getGender()));
                ps.setString(3,tUser.getId());
                ps.execute();
                tUser.setUserPwd(tUser.getRelUserPwd());
                jdbc.close();
                List<String> list=tUser.getLikes();
                if(list!=null){
                    conn=jdbc.getConnection();
                    String insertLikeSql="insert into t_user_like values(?,?,?)";
                    ps=conn.prepareStatement(insertLikeSql);


                    for(String s:list){
                        ps.setString(1, UUID.randomUUID().toString().replace("-",""));
                        ps.setString(2,tUser.getId());
                        ps.setString(3,s);
                        ps.addBatch();
                    }
                }

                ps.executeBatch();
                return tUser;
            }catch(SQLException e){
                e.printStackTrace();
            }finally {
                jdbc.close();
            }


            return null;
        }

        public T_User selectT_User(String id){
            Connection conn=jdbc.getConnection();
            String selectSQl="select id,username,userpwd,email,gender,`explain` from t_user where id = ?";
            try{
                PreparedStatement ps=conn.prepareStatement(selectSQl);
                ps.setString(1,id);
                ResultSet rs=ps.executeQuery();
                if(rs.next()){
                    T_User tUser=new T_User();
                    tUser.setId(id);
                    tUser.setUserName(rs.getString("username"));
                    tUser.setUserPwd(rs.getString("userpwd"));
                    tUser.setRelUserPwd(rs.getString("userpwd"));
                    tUser.setEmail(rs.getString("email"));
                    tUser.setGender(rs.getString("gender"));
                    tUser.setExplain(rs.getString("explain"));
                    return tUser;
                }
            }catch (SQLException e){
                e.printStackTrace();
            }finally {
                jdbc.close();
            }
            return null;

        }

}

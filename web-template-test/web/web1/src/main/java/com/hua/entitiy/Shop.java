package com.hua.entitiy;

/**
 * Created by Administrator on 2016/11/16.
 */
public class Shop implements Comparable<Shop> {
    private String merchantName;
    private String ShopName;
    private String gender;
    private int number=1;
    @Override
    public int compareTo(Shop o) {
        return o.getMerchantName().hashCode()-this.merchantName.hashCode();
    }

    public void setMerchantName(String merchantName) {
        this.merchantName = merchantName;
    }

    public String getMerchantName() {
        return merchantName;
    }

    public int getNumber(){
        return this.number;
    }
    public void addNUmber(){
        this.number++;
    }

    public void setShopName(String shopName) {
        ShopName = shopName;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getShopName() {

        return ShopName;
    }

    public String getGender() {
        return gender;
    }

    public Shop(String shopName, String gender) {
        ShopName = shopName;
        this.gender = gender;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Shop shop = (Shop) o;

        if (ShopName != null ? !ShopName.equals(shop.ShopName) : shop.ShopName != null) return false;
        return gender != null ? gender.equals(shop.gender) : shop.gender == null;

    }

    @Override
    public int hashCode() {
        int result = ShopName != null ? ShopName.hashCode() : 0;
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Shop{" +
                "merchantName='" + merchantName + '\'' +
                ", ShopName='" + ShopName + '\'' +
                ", gender='" + gender + '\'' +
                ", number=" + number +
                '}';
    }
}

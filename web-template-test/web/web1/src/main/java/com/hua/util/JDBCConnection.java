package com.hua.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by Administrator on 2016/11/17.
 */
public class JDBCConnection {

    private Connection conn;
    public Connection getConnection(){
        try{
            Class.forName("com.mysql.jdbc.Driver");
            conn= DriverManager.getConnection("jdbc:mysql:///m?useUnicode=true&characterEncoding=utf8","root","201314");
            return conn;
        }catch(Exception e){
            e.printStackTrace();
        }

        return null;
    }
    public void close(){
        if(conn!=null){
            try{
                conn.close();
            }catch(Exception e){
                e.printStackTrace();
            }

        }
    }

}

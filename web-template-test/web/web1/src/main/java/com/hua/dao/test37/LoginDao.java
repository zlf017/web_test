package com.hua.dao.test37;

import com.hua.entitiy.test37.T_User;
import com.hua.util.JDBCConnection;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2016/11/17.
 */
public class LoginDao {
    JDBCConnection jdbc=new JDBCConnection();
    public T_User login(String userName,String userPwd){
        Connection conn=jdbc.getConnection();
        String sql="select * from t_user where username=?";
        try{
            PreparedStatement ps=conn.prepareStatement(sql);
            ps.setString(1,userName);
            ResultSet rs=ps.executeQuery();
            T_User tUser=new T_User();
            if(rs.next()){
                tUser.setId(rs.getString("id"));
                tUser.setUserName(rs.getString("username"));
                tUser.setUserPwd(rs.getString("userPwd"));
                String gender=rs.getString("gender");
                tUser.setGender(gender);
                tUser.setEmail(rs.getString("email"));
                tUser.setExplain(rs.getString("explain"));
            }
            jdbc.close();
            conn=jdbc.getConnection();
            String sql1="select * from t_user_like where userid='"+tUser.getId()+"'";
            Statement statement=conn.createStatement();
            rs=statement.executeQuery(sql1);
            List<String> list=new ArrayList<>();
            while(rs.next()){
                list.add(rs.getString("like"));
            }
            tUser.setLikes(list);
            return tUser;
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            jdbc.close();
        }
        return null;
    }
}

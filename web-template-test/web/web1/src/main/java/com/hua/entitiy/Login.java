package com.hua.entitiy;

import java.util.Date;

/**
 * Created by Administrator on 2016/11/16.
 */
public class Login {
    private String loginName;
    private String date;

    public Login(String loginName, String date) {
        this.loginName = loginName;
        this.date = date;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLoginName() {

        return loginName;
    }

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "Login{" +
                "loginName='" + loginName + '\'' +
                ", date='" + date + '\'' +
                '}';
    }
}

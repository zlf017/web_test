package com.hua.servlet.test37;

import com.hua.entitiy.test37.T_User;
import com.hua.service.test37.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Administrator on 2016/11/17.
 */
public class LoginServlet extends HttpServlet {
    private LoginService ls=new LoginService();
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String userName=req.getParameter("userName");
        String userPwd=req.getParameter("userPwd");
        T_User t_user=ls.login(userName,userPwd);
//        System.out.println(t_user.getLikes());
        if(t_user!=null&&t_user.getUserPwd().equals(userPwd)){
            HttpSession session=req.getSession();
            session.setAttribute("tUser",t_user);
            resp.sendRedirect("test37/practise1_4.jsp");
            req.getSession().setAttribute("loginError","");
        }else{
            req.getSession().setAttribute("loginError","用户名或密码错误");
            resp.sendRedirect("test37/practise1_3.jsp");
        }

    }
}

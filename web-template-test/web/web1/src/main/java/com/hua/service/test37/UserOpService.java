package com.hua.service.test37;

import com.hua.dao.test37.LoginDao;
import com.hua.dao.test37.UserOpDao;
import com.hua.entitiy.test37.T_User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Administrator on 2016/11/17.
 */
public class UserOpService {

    private UserOpDao ud=new UserOpDao();
    private RegistService rs=new RegistService();

    public T_User updateT_User(T_User tUser, String oldPwd, HttpServletRequest req){
        if(!rs.valiUserPwd(oldPwd)&& rs.valiRegist(tUser,req) ){

            return null;
        }

        T_User t=ud.selectT_User(tUser.getId());

        if(t!=null&&t.getUserPwd().equals(tUser.getUserPwd())){
            T_User b1=ud.updateUser(tUser);
            System.out.println("----null---"+t);
            return b1;
        }else{
            System.out.println("----null---ll");
            return null;
        }

    }

}

package com.hua.service.test37;

/**
 * Created by Administrator on 2016/11/17.
 */

import com.hua.dao.test37.RegistDao;
import com.hua.entitiy.test37.T_User;

import javax.servlet.http.HttpServletRequest;

/**
 * 注册数据验证
 */
public class RegistService {
    RegistDao rd = new RegistDao();

    public boolean regist(T_User tUser,HttpServletRequest req) {
        if (valiRegist(tUser,req)) {
            rd.regist(tUser);
            return true;
        }
        return false;
    }

    public boolean valiRegist(T_User tUser, HttpServletRequest req) {
        if (valiUserName(tUser.getUserName())  ) {

            if(valiUserPwd(tUser.getUserPwd())){
                if(tUser.getUserPwd().equals(tUser.getRelUserPwd())){
                    if(valiGender(tUser.getGender())){
                        if(isEmailValid(tUser.getEmail())){
                            return true;
                        }else{
                            req.setAttribute("message","邮箱不合法");
                        }
                    }else{
                        req.setAttribute("message","性别不合法");
                    }
                }else{
                    req.setAttribute("message","两次密码不一样");
                }
            }else{
                req.setAttribute("message","你输入的密码格式不合法");
            }
        }else{
            req.setAttribute("message","用户名不合法");
        }
        return false;
    }
    /**
     * 邮箱验证
     * 1.必须是数字、字母、下划线、@、.组成
     * 2.邮箱必须满足格式a@b.c
     * 其中：
     * a.必须是数字、字母、下滑线
     * b.必须是字母或数字。
     * c.字母和点组成  列如：aaa@cc.deu.com
     *
     * @param email
     * @return
     */
    public boolean isEmailValid(String email) {
        if("".equals(email)){
            return true;
        }
        String[] emails = email.split("@");
        if (emails.length != 2) {
            return false;
        }
        //判断a部分
        if (!valiString(emails[0])) {
            return false;
        }
        //判断b部分
        String[] emailSuffixs = emails[1].split("\\.");
        if (emailSuffixs.length <= 1) {
            return false;
        }
        if (!isContainLetterOrNumber(emailSuffixs[0])) {
            return false;
        }

        //判断c部分
        for (int i = 1; i < emailSuffixs.length; i++) {
            if (!isContainLetter(emailSuffixs[i])) {
                return false;
            }
        }
        return true;
    }

    /**
     * p判断是否是字母或数字组成
     * * @param str
     * @return
     */
    public boolean isContainLetterOrNumber(String str) {
        String nums = "1234567890";
        String words = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLzXCVBNM";
        int n = 0;
        int w = 0;
        char[] cs=str.toCharArray();
        for(char c:cs){
            if(nums.indexOf(c)>=0){
                n=1;
            }else if(words.indexOf(c)>=0){
                w=1;
            }
        }
        if((n+w)==1){
            return true;
        }
        return false;
    }
    /**
     * 判断字符串是否是由字母组成的
     *
     * @param str
     * @return
     */
    public boolean isContainLetter(String str) {
        int length = str.length();
        for (int i = 0; i < length; i++) {
            char c = str.charAt(i);
            int val = (int) c;
            if (val >= 65 && val <= 90) {
                continue;
            } else if (val >= 97 && val <= 122) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }

    public boolean valiGender(String ch){
        if(ch == "0"||ch=="1" ){
            return true;
        }
        return false;
    }

    public boolean valiUserPwd(String userPwd) {
        if (userPwd == null || userPwd.trim().equals("")) {
            return false;
        }
        if (userPwd.length() < 6 || userPwd.length() > 10) {
            return false;
        }
        if (valiStringPwd(userPwd)) {

            return valiStringContainNumberandWord(userPwd);
        }
        return false;

    }

    public boolean valiUserName(String userName) {
        if (userName == null || userName.trim().equals("")) {
            return false;
        }
        if (userName.length() < 4 || userName.length() > 10) {
            return false;
        }

        if (valiString(userName)) {
            return rd.valiUserName(userName);
        }
        return false;
    }

    /**
     * 验证字符串是否是由数字、字母、下划线组成
     *
     * @return
     */
    private boolean valiString(String str) {
        String strMode = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM_";
        char[] chars = str.toCharArray();
        for (char c : chars) {
            if (strMode.indexOf(c) == -1) {
                return false;
            }
        }
        return true;
    }

    /**
     * 验证字符串是否是由数字、字母组成
     *
     * @return
     */
    private boolean valiStringPwd(String str) {
        String strMode = "1234567890qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM";
        char[] chars = str.toCharArray();
        for (char c : chars) {
            if (strMode.indexOf(c) == -1) {
                return false;
            }
        }
        return true;
    }

    /**
     * @param userPwd
     * @return
     */
    private boolean valiStringContainNumberandWord(String userPwd) {
        String nums = "1234567890";
        String words = "qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLzXCVBNM";
        boolean n = false;
        boolean w = false;
        char[] chars = userPwd.toCharArray();
        for (char c : chars) {
            if (nums.indexOf(c) >= 0) {
                n = true;
            }
            if (words.indexOf(c) >= 0) {
                w = true;
            }
        }
        if (n && w) {
            return true;
        }
        return false;
    }

}

package com.hua.servlet.test37;

import com.hua.entitiy.test37.T_User;
import com.hua.service.test37.RegistService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by Administrator on 2016/11/17.
 */
public class RegistServlet extends HttpServlet {
    RegistService rs=new RegistService();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd=req.getRequestDispatcher("/test37/practise1_2.jsp");
        String userName=req.getParameter("userName");
        String userPwd=req.getParameter("userPwd");
        String relUserPwd=req.getParameter("relUserPwd");
        String gender=req.getParameter("gender");
        String email=req.getParameter("email");
        String[] likes=req.getParameterValues("like");
        String explain=req.getParameter("explain");
        T_User t_user=new T_User();
        t_user.setUserName(userName);
        t_user.setUserPwd(userPwd);
        t_user.setRelUserPwd(relUserPwd);
        if( gender!=null&&gender.length()==1){
            t_user.setGender(gender);
        }else{
            req.setAttribute("regist","false");
            rd.forward(req,resp);
        }
        req.setAttribute("likeArray",likes);
        t_user.setEmail(email);
        t_user.setExplain(explain);
        if(likes!=null)t_user.setLikes(Arrays.asList(likes));

        System.out.println("ssss:"+t_user.getLikes().size());
        if(rs.regist(t_user,req)){
            //resp.sendRedirect("practise1_1.jsp");
            req.setAttribute("regist","true");
            rd.forward(req,resp);
        }else{
            req.setAttribute("regist","false");

            rd.forward(req,resp);
        }
    }


}

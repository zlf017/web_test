package com.hua.entitiy;

/**
 * Created by Administrator on 2016/11/17.
 */
public class Student {
    private String name;
    private int age;
    private char gender;
    private boolean b;
    private long l;
    private float f;
    private String[] strs={"str0","str1"};
    private char[] cs={'0','1'};

    public void setStrs(String[] strs) {
        this.strs = strs;
    }

    public String[] getStrs() {
        return strs;
    }

    public void setCs(char[] cs) {
        this.cs = cs;
    }

    public char[] getCs() {

        return cs;
    }

    public Student(String name, int age, char gender, boolean b, long l, float f) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.b = b;
        this.l = l;
        this.f = f;
    }

    public void setName(String name) {

        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public void setB(boolean b) {
        this.b = b;
    }

    public void setL(long l) {
        this.l = l;
    }

    public void setF(float f) {
        this.f = f;
    }

    public String getName() {

        return name;
    }

    public int getAge() {
        return age;
    }

    public char getGender() {
        return gender;
    }

    public boolean isB() {
        return b;
    }

    public long getL() {
        return l;
    }

    public float getF() {
        return f;
    }
}

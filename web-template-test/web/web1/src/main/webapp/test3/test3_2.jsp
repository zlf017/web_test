<%@ page import="java.net.URLDecoder" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/16
  Time: 11:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="test3.css" />
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <script type="text/javascript">
        <%
        request.setCharacterEncoding("utf-8");
        %>
        function nextPage(){
            var userName="${param.userName}";
            userName="<%=
            URLDecoder.decode(request.getParameter("userName"),"utf-8")%>"
            window.location.href="test3_3.jsp?userName="+encodeURIComponent(userName);
        }

    </script>
</head>
<body>
<div class="divBody">
    <div class="divTitle">用户名</div><div class="divPage">1</div>
    <span class="userNameShow">${param.userName}</span>
    <c:set var="userName" scope="request">${param.userName}</c:set>
    <a class="next" href="javascript:nextPage();">去下一页</a>
</div>
</body>
</html>

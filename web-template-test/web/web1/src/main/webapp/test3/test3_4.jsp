<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/16
  Time: 12:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <link type="text/css" rel="stylesheet" href="test3.css" />
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <%
        request.setCharacterEncoding("utf-8");
    %>
    <script>
        window.onload=function(){
            document.getElementById("spanShowUserName").innerText=decodeURIComponent("${param.userName}");
        }
        function nextPage(){
            window.location.href="test3_5.jsp?userName="+encodeURIComponent("${param.userName}")+"&showName="+encodeURIComponent("${param.showName}");
        }

    </script>
</head>
<body>
<div class="divBody">
    <div class="divTitle">用户名</div><div class="divPage">1</div>
    <span class="userNameShow" id="spanShowUserName"></span>
    <div id="divShowName">昵称：${param.showName}</div>
    <div id="pageOp"><a href="javascript:nextPage();">下一页</a></div>
</div>
</body>
</html>

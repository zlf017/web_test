<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/16
  Time: 15:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>添加购物车</title>
    <%
        String clearAll=request.getParameter("clearAll");
        if(clearAll!=null&&"true".equals(clearAll)){
            session.setAttribute("ShopSet",null);
        }
    %>

    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <script type="text/javascript">
        function validateSubmit(){
                var f=document.getElementById("f");
                var shopNameLength=f.shopName.length;
                var b=false;
                for(var i=0;i<shopNameLength;i++){
                    if(b){
                        continue;
                    }
                    if(f.shopName[i].checked){
                        b=true;
                    }
                }

                if(b){
                    if(f.gender[0].checked){
                        return true;
                    }else if(f.gender[1].checked){
                        return true;
                    }else{
                        alert("请选择款式");
                        return false;
                    }
                }else{
                    alert("请选择商品");
                    return false;
                }

        }

    </script>
</head>
<body>
<div>
    <div>session id:<%=session.getId()%>
    </div>
    <hr>
    <form action="session3_2.jsp" method="get" onsubmit="return validateSubmit();" id="f">
        <span><b>请选择你要购买的商品</b></span><br>
        <input type="checkbox" name="shopName" value="jeans">jeans
        <input type="checkbox" name="shopName" value="T-shirt">T-shirt
        <input type="checkbox" name="shopName" value="Jacket">Jacket
        <input type="checkbox" name="shopName" value="Skirt">Skirt
        <br>
        <br>
        <span><b>请选择你的款式：</b></span><br>
        <input type="radio" name="gender" value="Man">Man
        <input type="radio" name="gender" value="Woman">Woman
        <br>
        <br>
        <input type="submit" value="提交">
    </form>
</div>
</div>
</body>
</html>

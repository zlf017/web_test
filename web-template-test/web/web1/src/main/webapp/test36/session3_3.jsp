<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/16
  Time: 16:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%
    request.setCharacterEncoding("utf-8");
%>
<html>
<head>
    <title>Title</title>
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <style>
        td{
            width:150px;
            height: 50px;
        }
    </style>
</head>
<body>
    <div>
        <span>session id:<%=session.getId()%></span>
        <hr><br>
        <span><b>你已经购买的商品有：</b></span>

        <table>
            <thead>
            <tr>
                <td>商家</td>
                <td>产品名字</td>
                <td>款式</td>
                <td>数量</td>
            </tr>
            </thead>
            <tbody>
            <c:set var="merchantName" value=""></c:set>
            <c:forEach items="${ShopSet}" var="shop">
                <c:if test="${merchantName != shop.merchantName}">
                    <tr>
                        <td>${shop.merchantName }</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </c:if>
                <c:set var="merchantName" value="${shop.merchantName }"></c:set>
                <tr>
                    <td></td>
                    <td>${shop.shopName}</td>
                    <td>${shop.gender}</td>
                    <td>${shop.number}</td>
                </tr>
            </c:forEach>
            </tbody>
        </table>

        <br>
        <br>
        <div>
            <span>你已经完成交易，你的货物将送达以下地址：</span><br>
            ${param.address}<br>
            <br>
            <a href="session3_1.jsp?clearAll=true">返回重新购物</a>
        </div>
    </div>
</body>
</html>

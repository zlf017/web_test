<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/16
  Time: 15:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="com.hua.entitiy.Shop" %>
<%@ page import="java.util.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>确认商品</title>
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <style>
        td{
            width:150px;
            height: 50px;
        }
    </style>
    <script type="text/javascript">
        function validateAddress(){
            var address=document.getElementById("address");
            if(""==address.value){
                alert("请输入收货地址");
                return false;
            }
            return true;
        }
    </script>
    <%
        String[] shopNames=request.getParameterValues("shopName");
        String gender=request.getParameter("gender");
        List<Shop> shopList =(List<Shop>)session.getAttribute("ShopSet");
        if(shopList ==null){
            List<Shop> list=new ArrayList<>();
            session.setAttribute("ShopSet",list);
        }else{
            shopList =(List<Shop>)session.getAttribute("ShopSet");
        }

        for(int i=0;i<shopNames.length;i++){
            Shop shop=new Shop(shopNames[i],gender);
            setMerchantName(shop);
            ss:
            if(shopList.contains(shop)){
                for(Shop sh: shopList){
                    if(sh.getShopName().equals(shop.getShopName())&&sh.getGender().equals(shop.getGender())){
                        sh.addNUmber();
                        break ss;
                    }
                }
            }else{
                shopList.add(shop);
            }
        }
        Collections.sort(shopList);

    %>
    <%!
        /**
         * 给shop对象设置商家
         * @param shop
         */
        public void setMerchantName(Shop shop){
            switch (shop.getShopName()){
                case "jeans":
                case "T-shirt":
                    shop.setMerchantName("官");
                    return;
                case "Jacket":
                    shop.setMerchantName("刘");
                    return ;
                case "Skirt":
                    shop.setMerchantName("张");
                    return;
            }
        }
    %>
</head>
<body>
    <div>
        <div>
            <span>session id:<%=session.getId()%></span>
            <hr>
            <br>
            <div>
                <span>你将要购买的商品有：</span>
                <table>
                    <thead>
                        <tr>
                            <td>产品名字</td>
                            <td>款式</td>
                            <td>数量</td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${ShopSet }" var="shop">
                            <tr>
                                <td>${shop.shopName}</td>
                                <td>${shop.gender}</td>
                                <td>${shop.number}</td>
                            </tr>
                        </c:forEach>
                    </tbody>

                </table>
                <div>
                    <form action="session3_3.jsp" method="post" onsubmit="return validateAddress();">
                    送货地址：<input id="address" type="text" name="address" style="width:200px">
                    <input type="submit" value="确认购买，进入结算" style="width: 150px;">
                    </form>
                </div>
                <br>
                <div>
                    <a href="session3_1.jsp">返回继续购买</a>&nbsp;<a href="session3_1.jsp?clearAll=true">返回产品选择页面，清空购物车</a>
                </div>
        </div>
    </div>
</body>
</html>

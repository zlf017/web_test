<%@ page import="com.hua.entitiy.Student" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/17
  Time: 22:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8"/>
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->

    <%
        request.getSession().setAttribute("st", new Student("zhang", 10, 'z', true, 100, 12.000f));
    %>
</head>
<body>
<div>String:<h1>
    <c:choose>
        <c:when test="${st.name !='zhang'}">true</c:when>
        <c:otherwise> false</c:otherwise>
    </c:choose>
</h1></div>

<div>int:<h1>
    <c:choose>
        <c:when test="${st.age !=10}">true</c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
</h1></div>

<div>char:<h1>
    <c:choose>
        <c:when test="${st.gender != 122 }">true </c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
    <span>(char 被转换成longl类型)</span>
</h1></div>

<div>boolean:<h1>
    <c:choose>
        <c:when test="${st.b != true}">true</c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
</h1></div>

<div>long:<h1>
    <c:choose>
        <c:when test="${st.l !=100}">true</c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
</h1></div>

<div>float:<h1>
    <c:choose>
        <c:when test="${st.f !=12}">true</c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
</h1></div>

<div>String[]:<h1>
    <c:choose>
        <c:when test="${st.strs[0] =='str0'}">true</c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
</h1></div>

<div>char[]:<h1>
    <c:choose>
        <c:when test="${st.cs[0] ==48}">true</c:when>
        <c:otherwise>false</c:otherwise>
    </c:choose>
</h1></div>


</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/17
  Time: 14:34
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>登录</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
</head>
<body>
    <h1>用户登录</h1>
    <hr><span style="color: red;">${loginError}</span>
    <br>
    <div>
        <form action="<c:url value='/login'/>">
        用户名：<input type="text" name="userName"><br>
        密码：<input type="password" name="userPwd"><br>
        <input type="submit" value="登录">
        </form>
        <br><br><br>
        <a href="practise1_1.jsp">重新注册</a>
    </div>
</body>
</html>

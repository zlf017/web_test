<%@ page import="com.hua.entitiy.test37.T_User" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/17
  Time: 16:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
</head>
<body>
    <span>恭喜你登录成功</span>
    <div style="color: red;">${message}
    </div>
    <hr>
    <div>
        <div>你输入的资料是：</div>
        <div>用户名：${tUser.userName}</div>
        <div>密码：${tUser.userPwd}</div>
        <div>性别:
            <%
                String gender=((T_User)request.getSession().getAttribute("tUser")).getGender();
                if(gender!=null&&gender.equals("01")){
                    out.println("女");
                }else if(gender!=null&&gender.equals("11")){
                    out.println("男");
                }
            %>

        </div>
        <div>Email：${tUser.email}</div>
        <div>爱好：
            <c:forEach var="li" items="${tUser.likes}">
                ${li}&nbsp;
            </c:forEach>
        </div>
        <div>个人说明：${tUser.explain}</div>
        <br>
        <div>
            <a href="practise1_5.jsp">修改个人资料</a>
        </div>
        <a href="practise1_1.jsp?clear=1">退出</a>
    </div>
</body>
</html>

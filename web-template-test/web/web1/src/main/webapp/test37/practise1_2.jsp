<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/17
  Time: 14:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->

</head>
<body>
    <c:set var="regist" value="${regist}"></c:set>
    <c:choose>
        <c:when test="${regist == true}">
            <span>恭喜你注册成功</span>
            <hr>
            <div>
                <div>你输入的资料是：</div>
                <div>用户名：${param.userName}</div>
                <div>密码：${param.userPwd}</div>
                <div>性别:
                    <c:choose>
                        <c:when test="${param.gender == '0'}">女</c:when>
                        <c:otherwise>男</c:otherwise>
                    </c:choose>

                </div>
                <div>Email：${param.email}</div>
                <div>爱好：
                    <c:forEach var="li" items="${likeArray}">
                        ${li}&nbsp;
                    </c:forEach>
                </div>
                <div>个人说明：${param.explain}</div>
                <div><a href="practise1_3.jsp">点击登录</a></div>
            </div>
        </c:when>
        <c:otherwise>

            ${message }
            <h1>请输入正确格式的数据</h1>
            <div><a href="practise1_1.jsp">返回重新注册</a></div>
        </c:otherwise>
    </c:choose>




</body>
</html>

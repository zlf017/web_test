<%@ page import="java.util.List" %>
<%@ page import="java.io.IOException" %>
<%@ page import="com.hua.entitiy.test37.T_User" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/17
  Time: 16:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
</head>
<body>
    <div><br>
        <form action="${pageContext.request.contextPath}/updateUser" method="get">
            <input type="hidden" name="id" value="${tUser.id }">
            <input type="hidden" name="userName" value="${tUser.userName }">
            <input type="hidden" name="email" value="${tUser.email }">
            <input type="hidden" name="explain" value="${tUser.explain }">
        <div>用户名：${tUser.userName}</div>
        <div>旧密码：<input type="password" name="oldUserPwd"></div>
        <div>新密码：<input type="password" name="newUserPwd"></div>
        <div>确认新密码：<input type="password" name="relnewUserPwd"></div>
        <div>性别：
            <%
                String gender=((T_User)request.getSession().getAttribute("tUser")).getGender();
                if(gender!=null&&gender.equals("11")){
                    out.print("<input type=\"radio\" name=\"gender\" value=\"1\" checked=\"checked\">男");
                    out.print("<input type=\"radio\" name=\"gender\" value=\"0\" >女");
                }else if(gender!=null&&gender.equals("01")){
                    out.println("<input type=\"radio\" name=\"gender\" value=\"1\" >男");
                    out.print("<input type=\"radio\" name=\"gender\" value=\"0\" checked=\"checked\">女");
                }
            %>



        </div>
        <div>email：${tUser.email }</div>
        <div>爱好：
            <%
                List<String> list=((T_User)request.getSession().getAttribute("tUser")).getLikes();
                printLikes(list,"足球",out);
                printLikes(list,"羽毛球",out);
                printLikes(list,"网球",out);
                printLikes(list,"篮球",out);
            %>
            <%!
                public void printLikes(List<String> list,String str,JspWriter out)throws IOException{
                    if(list.contains(str)){
                        out.print("<input type=\"checkbox\" name=\"like\" value=\""+str+"\" checked='checked' >"+str);
                    }else{
                        out.print("<input type=\"checkbox\" name=\"like\" value=\""+str+"\" >"+str);
                    }
                }
            %>

            <%--<input type="checkbox" name="like" value="羽毛球">羽毛球
            <input type="checkbox" name="like" value="网球" >网球
            <input type="checkbox" name="like" value="篮球">篮球--%>
        </div>
        <div>说明：${tUser.explain}</div>

        <input type="submit" value="确认修改"/>
        </form>
        <div>
            <a href="practise1_4.jsp">返回资料查看</a>
        </div>
    </div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/17
  Time: 9:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>注册</title>
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <style type="text/css">
        span{
            color:red;
        }
    </style>
    <script type="text/javascript">
        window.onload=function(){
           // document.getElementById("userName").onblur=validateUserName;
        }
        function validateUserName(){
            var userName=document.getElementById("userName").value;
            var data={type:'valiUserName',userName:userName};
            var x=ajax("${pageContext.request.contextPath}/test37/regist",data);
            x.onreadystatechange=function(){
                if (x.readyState == 4) {
                    var status = x.status;
                    if (status >= 200 && status < 300) {
                       var val=xhr.responseText;
                        console.log(val);
                    } else {
                        //error
                    }
                }
            }
        }
        function ajax(url,data){
            var x=getHttpRequest();
            x.open('get',url,false);
            x.send(data);
            return x;
        }
        function getHttpRequest(){
            var x=null;;
            if(window.XMLHttpRequest){
                x=new XMLHttpRequest();
            }else{
                x=new ActiveXObject("Microsoft.XMLHTTP");
            }
            return x;
        }
        <%
            String clear=request.getParameter("clear");
            if(clear!=null&&clear.equals("1")){
                session.setAttribute("tUser",null);
                session.setAttribute("message",null);
            }
        %>
    </script>
</head>
<body>
    <h1>用户注册</h1>
    <br>
    <br>
    <div>
        <form action="<c:url value="/test37/regist"/>"  id="formRegist">
        用户名：<span>*</span><input type="text" id="userName" name="userName" maxlength="20"><span class="error">4-10个字符(只能由数字、字母、下划线组成)</span><br>
        密码：<span>*</span><input type="password" id="userPwd" name="userPwd" maxlength="10"><span class="error">6-10个字符(只能由数字和字母组成，且必须同时包含数字和字母)</span><br>
        确认密码：<span>*</span><input type="password" id="relUserPwd" name="relUserPwd" maxlength="10"><span class="error"></span><br>
        性别：<span>*</span><input type="radio" name="gender" value="1">男
        <input type="radio" name="gender" value="0">女<br><span class="error"></span>
        Email：<input type="text" name="email" maxlength="50" ><br>
        爱好：<input type="checkbox" name="like" value="足球" >足球
        <input type="checkbox" name="like" value="羽毛球">羽毛球
        <input type="checkbox" name="like" value="网球" >网球
        <input type="checkbox" name="like" value="篮球">篮球<br>
        个人说明：<textarea type="range" name="explain" style="width: 200px;height: 200px" maxlength="200"></textarea><br>
        <input type="submit" value="注册">
        </form>
        <br><a href="practise1_3.jsp">登录</a>
    </div>
</body>
</html>

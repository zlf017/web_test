<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="com.hua.entitiy.Login" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2016/11/16
  Time: 13:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <!-- <link type="text/css" rel="stylesheet" href="test3.css" />-->
    <!--<script type="text/javascript" src="<c:url value="/js/jquery-1.5.1.js" />">
    </script>-->
    <script type="text/javascript">
        function openNewWindow(){
            window.open("<c:url value="/test31/session3.jsp"/>","登录用户历史");
        }
    </script>
</head>
<body>
<%
    Date date=new Date();
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dataFormat=sdf.format(date);
    String sessionId=session.getId();
    request.setAttribute("sessionId",sessionId);
    request.setAttribute("dataFormat",dataFormat);


%>

<div>session id: ${sessionId }</div>
<hr>

<div>
    <div><span>当前进入用户：${param.userName}  -当前时间：${dataFormat} </span></div>
    <br>
    <br>
    <div>
        <span>你之前登录的用户有</span>
        <br>
        <br>
            <c:if test="${loginUserList!=null}">

                <c:forEach items="${loginUserList}" var="login">
                    <c:out value="${login.loginName}"/>--
                    <c:out value="${login.date}"/><br>
                </c:forEach>
            </c:if>
        <div>
        <%
            List<Login> obj=(List<Login>)application.getAttribute("loginUserList");
            if(obj==null){
                List<Login> list=new ArrayList<>();
                Login login=new Login(request.getParameter("userName"),dataFormat);
                list.add(login);
                application.setAttribute("loginUserList",list);
            }else{
                obj.add(new Login(request.getParameter("userName"),dataFormat) );
                //application.setAttribute("loginUserList",obj);
            }
           // System.out.println("------------------");
        %>
        </div>
        <div>
            <a href="session1.jsp">更换账户登录</a>
            <a href="javascript:openNewWindow()">在新窗口中查看历史登录记录</a>

        </div>
    </div>
</div>
</body>
</html>

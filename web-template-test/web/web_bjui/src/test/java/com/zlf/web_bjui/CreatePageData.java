package com.zlf.web_bjui;

import org.junit.Test;

import com.zlf.web_bjui.servlet.util.JDBCUtil;

public class CreatePageData {
	
	@Test
	public void createTable() throws Exception{
		JDBCUtil jdbc=new JDBCUtil();
		jdbc.getConnection();
		String sql="CREATE TABLE `page_test` (`id` int(11) NOT NULL AUTO_INCREMENT,`name` varchar(32) NOT NULL,`age` int(11) DEFAULT NULL,  `log` varchar(32) DEFAULT NULL,  PRIMARY KEY (`id`))";
		jdbc.execute(sql);
		jdbc.close();
	}
	@Test
	public void insertData() throws Exception{
		JDBCUtil jdbc=new JDBCUtil();
		jdbc.getConnection();
		StringBuilder sql=new StringBuilder("insert into page_test (name,age,log) values ");
		for(int i=1;i<400;i++){
			sql.append("( 'name"+i);
			sql.append("',"+i);
			sql.append(",'sadasdasd"+i);
			sql.append("'),");
		}
		jdbc.execute(sql.toString().substring(0, sql.toString().length()-1));
		jdbc.close();
	}
}

<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>index</title>
</head>
<body>
<a href="01form.jsp">form</a><br>
<a href="02DataGridtables.jsp">02DataGridtables</a><br>
<a href="03editTables.jsp">03editTables</a><br>
<a href="03tables.jsp">03tables.jsp</a><br>

<a href="04textarea.jsp">富文本框.jsp</a><br>

<a href="05tree.jsp">05tree.jsp 树结构数据显示</a><br>

<a href="06download.jsp">06download.jsp</a><br>
<a href="07upload.jsp">07upload.jsp</a><br>
<a href="page">08page.jsp</a><br>
<a href="09Ajax.jsp">09Ajax.jsp 发送ajax刷新页面</a><br>
<a href="10dialog.jsp">10dialog.jsp 弹出对话窗</a><br>

<a href="11alertmsg.jsp">11alertmsg.jsp</a><br>
<a href="12search.jsp">12search.jsp 搜索ajax（有问题）</a><br>

<a href="13ajaxLoad.jsp">13ajaxLoad.jsp</a><br>

<a href="highcharts.jsp">图形实例</a>

<br>
<br>
<a href="bjui-structure/login_success.jsp">功能主页</a>
</body>
</html>
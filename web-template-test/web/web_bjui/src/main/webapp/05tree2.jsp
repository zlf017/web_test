<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/1/24
  Time: 10:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<html>
<head>
    <title>输的新增编辑修改</title>
    <%@include file="init.jsp"%>
</head>
<body>
    <div style="width: 50%;height: 100%;">
        <ul data-toggle="ztree" class="ztree">
            <li data-id="1" data-pid="0" data-faicon="rss" data-faicon-close="cab">表单元素</li>
            <li data-id="10" data-pid="1" data-url="form-button.html" data-tabid="form-button" data-faicon="bell">按钮</li>
            <li data-id="11" data-pid="1" data-url="form-input.html" data-tabid="form-input" data-faicon="info-circle">文本框</li>
            <li data-id="2" data-pid="0" data-faicon="rss" data-faicon-close="cab">新添加</li>
        </ul>
    </div>
    <div style="width: 50%;height: 100%;">
        <input name="treeName" type="text">
        <ul class="bjui-pageFooter">
            <li><button type="submit">保存</button></li>
        </ul>
    </div>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/1/18
  Time: 14:12
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    request.setAttribute("root", request.getContextPath());
%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="zh">
<head>
    <title>bjui</title>
    <%@include file="../init.jsp" %>
</head>
<body>

<header id="bjui-header">
    <!-- logo --><!-- 快捷菜单(消息、用户信息、切换皮肤)
    <div class="bjui-hnav-more-left">
       左边
    </div>
    <div class="bjui-hnav-more-right">
        右边
    </div>-->
    <div class="bjui-navbar-header">
        <button type="button" class="bjui-navbar-toggle btn-default" data-toggle="collapse"
                data-target="#bjui-navbar-collapse">
            <i class="fa fa-bars"></i>
        </button>
        <a class="bjui-navbar-logo" href="#">
            <img src="${root}/img/logo.png">
        </a>
    </div>
    <nav id="bjui-navbar-collapse">
        <ul class="bjui-navbar-right" >
            <li class="datetime" style="float: left;height: 35px;">
                <div><span id="bjui-date"></span><i class="fa fa-clock-o" style="margin-left: 13px;"></i> <span id="bjui-clock"></span></div>
            </li>
            <li><a href="http://www.bootcss.com/" target="_blank">Bootstrap中文网</a></li>
            <li><a href="http://www.j-ui.com/" target="_blank">DWZ(j-ui)官网</a></li>
            <li><a href="#">消息 <span class="badge">4</span></a></li>
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">我的账户 <span
                    class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="changepwd.html" data-toggle="dialog" data-id="changepwd_page" data-mask="true"
                           data-width="400" data-height="260">&nbsp;<span class="glyphicon glyphicon-lock"></span> 修改密码&nbsp;
                    </a></li>
                    <li><a href="#">&nbsp;<span class="glyphicon glyphicon-user"></span> 我的资料</a></li>
                    <li class="divider"></li>
                    <li><a href="login.html" class="red">&nbsp;<span class="glyphicon glyphicon-off"></span> 注销登陆</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown"><a href="#" class="dropdown-toggle theme purple" data-toggle="dropdown"><i
                    class="fa fa-tree"></i></a>
                <ul class="dropdown-menu" role="menu" id="bjui-themes">
                    <li><a href="javascript:;" class="theme_default" data-toggle="theme" data-theme="default">&nbsp;<i
                            class="fa fa-tree"></i> 黑白分明&nbsp;&nbsp;</a></li>
                    <li><a href="javascript:;" class="theme_orange" data-toggle="theme" data-theme="orange">&nbsp;<i
                            class="fa fa-tree"></i> 橘子红了</a></li>
                    <li class="active"><a href="javascript:;" class="theme_purple" data-toggle="theme"
                                          data-theme="purple">&nbsp;<i class="fa fa-tree"></i> 紫罗兰</a></li>
                    <li><a href="javascript:;" class="theme_blue" data-toggle="theme" data-theme="blue">&nbsp;<i
                            class="fa fa-tree"></i> 青出于蓝</a></li>
                    <%--<li><a href="javascript:;" class="theme_red" data-toggle="theme" data-theme="red">&nbsp;<i class="fa fa-tree"></i> 红红火火</a></li>--%>
                    <li><a href="javascript:;" class="theme_green" data-toggle="theme" data-theme="green">&nbsp;<i
                            class="fa fa-tree"></i> 绿草如茵</a></li>
                </ul>
            </li>
        </ul>
    </nav>


    <div id="bjui-hnav">
        <ul id="bjui-hnav-navbar">
            <!-- 欢迎 -->
            <li style="width:204px;"><a>欢迎您，超级管理员！</a></li>
            <!-- zTree菜单 - BEGIN -->
            <!--  active 默认选中 -->
            <li class="active">
                <a href="javascript:;" data-toggle="slidebar">表单元素</a>
                <div class="items hide" data-noinit="true">
                    <ul class="menu-items" data-faicon="hand-o-up">
                        <li><a href="navtab/index.jsp" data-options="{id:'form-input', faicon:'terminal'}"
                               data-toggle="navtab">文本框</a></li>
                        <li><a href="form-input.html" data-options="{id:'form-input', faicon:'terminal'}">文本框2</a></li>
                        <li><a href="form-input.html" data-options="{id:'form-input', faicon:'terminal'}">文本框3</a></li>
                    </ul>

                </div>
            </li><!-- zTree菜单 - END -->
            <!-- 列表菜单 - BEGIN -->
            <li>
                <a href="javascript:;" data-toggle="slidebar">
                    <i class="fa fa-cog"></i>
                    表单元素&演示
                </a>
                <div class="items hide" data-noinit="true">
                    <ul class="menu-items" data-faicon="hand-o-up">
                        <li>
                            <a href="javascript:alert('s')" data-options="{id:'form-button', faicon:'hand-o-up'}">按钮</a>
                            <b><i class="fa fa-angle-down"></i></b>
                            <!-- 子级菜单 -->
                            <ul class="menu-items-children">
                                <li><a href="form-input.html"
                                       data-options="{id:'form-input', faicon:'terminal'}">文本框</a></li>
                                <li><a href="form-select.html"
                                       data-options="{id:'form-select', faicon:'caret-square-o-down'}">下拉选择框</a></li>
                            </ul>
                        </li>
                        <li><a href="form-input.html" data-options="{id:'form-input', faicon:'terminal'}">文本框</a></li>
                        <li><a href="form-select.html" data-options="{id:'form-select', faicon:'caret-square-o-down'}">下拉选择框</a>
                        </li>
                        <li><a href="form-checkbox.html" data-options="{id:'form-checkbox', faicon:'check-square-o'}">复选、单选框</a>
                        </li>
                    </ul>
                    <ul class="menu-items" data-title="表单Demo" data-faicon="list">
                        <li><a href="form.html" data-options="{id:'form-demo', faicon:'th-large'}">表单示例</a></li>
                    </ul>
                </div>
            </li><!-- zTree菜单 - End -->
            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-cog"></i>
                系统设置
                <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu" style="min-width: 130px;">
                    <li><a href="#">角色权限</a></li>
                    <li><a href="#">用户列表</a></li>
                    <li class="divider"></li>
                    <li><a href="#">关于我们</a></li>
                    <li class="divider"></li>
                    <li><a href="#">友情链接</a></li>
                </ul>
            </li>
            <!-- 下拉菜单 - BEGIN -->
            <%-- <li>
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-cog"></i>
                    下拉菜单</a>
                <span class="caret"></span>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#">下拉菜单</a></li>
                    <li><a href="#">下拉菜单二 </a></li>
                </ul>
            </li>--%><!-- 下拉菜单 - END -->
        </ul>
    </div>
</header>
<div class="bjui-window"></div>
<div id="bjui-container">
    <div class="bjui-leftside" >
        <div id="bjui-sidebar-s" >
            <div class="collapse"></div>
        </div>
        <div id="bjui-sidebar" >
            <div class="toggleCollapse"><h2><i class="fa fa-bars"></i> 导航栏 <i class="fa fa-bars"></i></h2><a
                    href="javascript:;" class="lock" data-original-title="" title=""><i class="fa fa-lock"></i></a>
            </div>
            <div class="panel-group panel-main" data-toggle="accordion" id="bjui-accordionmenu"
                 data-heightbox="#bjui-sidebar" data-offsety="26">

            </div>
        </div>
    </div>
    <div class="tablePage" id="bjui-navtab" width="100%" style="min-width: 1100px">

        <div class="tabsPageHeader" style="width: 100%">
            <div class="tabsPageHeaderContent">
                <ul class="navtab-tab nav nav-tabs">
                    <li><a href="${root}/bjui-structure/navtab/index.jsp"><span>我的主页</span></a></li>
                </ul>
            </div>
            <div class="tabsLeft"><i class="fa fa-angle-double-left"></i></div>
            <div class="tabsRight"><i class="fa fa-angle-double-right"></i></div>
            <div class="tabsMore"><i class="fa fa-angle-double-down"></i></div>
        </div>
        <ul class="tabsMoreList">
            <li><a href="javascript:;">我的主页</a></li>
            <li><a href="javascript:;">我的主页1</a></li>
            <li><a href="javascript:;">我的主页2</a></li>
        </ul>
        <%-- <div class="navtab-panel tabsPageContent">
             <div class="page unitBox">

             </div>
         </div>--%>
        <div class="navtab-panel tabsPageContent">
            <div class="navtabPage unitBox">
                <div class="bjui-pageContent" style="background:red;">
                    Loading...
                </div>
            </div>
        </div>

    </div>
</div>
<footer class="bjui-pageFooter">
    <!--  页脚本  -->
    pageFooter
</footer>

</body>
</html>

<script>
    //时钟
    var today = new Date(), time = today.getTime()
    $('#bjui-date').html(today.formatDate('yyyy/MM/dd'))
    setInterval(function() {
        today = new Date(today.setSeconds(today.getSeconds() + 1))
        $('#bjui-clock').html(today.formatDate('HH:mm:ss'))
    }, 1000)

</script>
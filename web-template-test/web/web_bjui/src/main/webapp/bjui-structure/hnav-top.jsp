<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2017/1/18
  Time: 15:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<div id="bjui-hnav">
    <button type="button" class="bjui-hnav-toggle btn-default" data-toggle="collapse" data-target="#bjui-hnav-navbar">
        <i class="fa fa-bars"></i>
    </button>
    <ul id="bjui-hnav-navbar">
        <!-- 欢迎 -->
        <li style="width:204px;"><a>欢迎您，超级管理员！</a></li>

        <!-- zTree菜单 - BEGIN -->
        <li class="active"><a href="javascript:;" data-toggle="slidebar">表单元素</a>
            <div class="items hide" data-noinit="true">
                <ul id="bjui-hnav-tree-input" class="ztree ztree_main" data-toggle="ztree" data-on-click="MainMenuClick" data-expand-all="true" data-faicon="check-square-o" data-title="基本元素">
                    <li data-id="1" data-pid="0" data-faicon="folder-open-o" data-faicon-close="folder-o">表单元素</li>
                    <li data-id="10" data-pid="1" data-url="form-button.html" data-tabid="form-button" data-faicon="hand-o-up">按钮</li>
                    <li data-id="11" data-pid="1" data-url="form-input.html" data-tabid="form-input" data-faicon="terminal">文本框</li>
                    <li data-id="12" data-pid="1" data-url="form-select.html" data-tabid="form-select" data-faicon="caret-square-o-down">下拉选择框</li>
                    <li data-id="13" data-pid="1" data-url="form-checkbox.html" data-tabid="table" data-faicon="check-square-o">复选、单选框</li>

                </ul>
                <ul id="bjui-hnav-tree-form" class="ztree ztree_main" data-toggle="ztree" data-on-click="MainMenuClick" data-expand-all="true" data-faicon="list" data-title="综合演示">
                    <li data-id="1" data-pid="0" data-faicon="folder-open-o" data-faicon-close="folder-o">表单演示</li>
                    <li data-id="14" data-pid="1" data-url="form.html" data-tabid="form" data-faicon="list">表单综合演示</li>
                </ul>
            </div>
        </li><!-- zTree菜单 - END -->

        <!-- 列表菜单 - BEGIN -->
        <li><a href="javascript:;" data-toggle="slidebar">表单元素&演示</a>
            <div class="items hide" data-noinit="true">
                <ul class="menu-items" data-faicon="hand-o-up">
                    <li><a href="form-button.html" data-options="{id:'form-button', faicon:'hand-o-up'}">按钮</a><b><i class="fa fa-angle-down"></i></b>
                        <!-- 子级菜单 -->
                        <ul class="menu-items-children">
                            <li><a href="form-input.html" data-options="{id:'form-input', faicon:'terminal'}">文本框</a></li>
                            <li><a href="form-select.html" data-options="{id:'form-select', faicon:'caret-square-o-down'}">下拉选择框</a></li>
                        </ul>
                    </li>
                    <li><a href="form-input.html" data-options="{id:'form-input', faicon:'terminal'}">文本框</a></li>
                    <li><a href="form-select.html" data-options="{id:'form-select', faicon:'caret-square-o-down'}">下拉选择框</a></li>
                    <li><a href="form-checkbox.html" data-options="{id:'form-checkbox', faicon:'check-square-o'}">复选、单选框</a></li>
                </ul>
                <ul class="menu-items" data-title="表单Demo" data-faicon="list">
                    <li><a href="form.html" data-options="{id:'form-demo', faicon:'th-large'}">表单示例</a></li>
                </ul>
            </div>
        </li><!-- zTree菜单 - End -->

        <!-- 下拉菜单 - BEGIN -->

        <li><a href="#" class="dropdown-toggle" data-toggle="dropdown"><!-- 下拉菜单 --></a>

            <ul class="dropdown-menu" role="menu">
                <li><a href="#"><!-- 下拉菜单一 --></a></li>
                <li><a href="#"><!-- 下拉菜单二 --></a></li>
            </ul>
        </li><!-- 下拉菜单 - END -->
    </ul>
</div>

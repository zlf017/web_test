<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>form</title>
<!-- bootstrap - css -->
<link href="BJUI/themes/css/bootstrap.css" rel="stylesheet">
<!-- core - css -->
<link href="BJUI/themes/css/style.css" rel="stylesheet">
<link href="BJUI/themes/blue/core.css" id="bjui-link-theme" rel="stylesheet">
<!-- plug - css -->
<link href="BJUI/plugins/kindeditor_4.1.10/themes/default/default.css" rel="stylesheet">
<link href="BJUI/plugins/colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
<link href="BJUI/plugins/niceValidator/jquery.validator.css" rel="stylesheet">
<link href="BJUI/plugins/bootstrapSelect/bootstrap-select.css" rel="stylesheet">
<link href="BJUI/themes/css/FA/css/font-awesome.min.css" rel="stylesheet">

<!-- jquery -->
<script src="BJUI/js/jquery-1.7.2.min.js"></script>
<script src="BJUI/js/jquery.cookie.js"></script>
<!--[if lte IE 9]>
<script src="BJUI/other/jquery.iframe-transport.js"></script>    
<![endif]-->
<!-- BJUI.all 分模块压缩版 -->
<script src="BJUI/js/bjui-all.js"></script>

<!-- plugins -->
<!-- swfupload for uploadify && kindeditor -->
<script src="BJUI/plugins/swfupload/swfupload.js"></script>
<!-- kindeditor -->
<script src="BJUI/plugins/kindeditor_4.1.10/kindeditor-all.min.js"></script>
<script src="BJUI/plugins/kindeditor_4.1.10/lang/zh_CN.js"></script>
<!-- colorpicker -->
<script src="BJUI/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- ztree -->
<script src="BJUI/plugins/ztree/jquery.ztree.all-3.5.js"></script>
<!-- nice validate -->
<script src="BJUI/plugins/niceValidator/jquery.validator.js"></script>
<script src="BJUI/plugins/niceValidator/jquery.validator.themes.js"></script>
<!-- bootstrap plugins -->
<script src="BJUI/plugins/bootstrap.min.js"></script>
<script src="BJUI/plugins/bootstrapSelect/bootstrap-select.min.js"></script>
<script src="BJUI/plugins/bootstrapSelect/defaults-zh_CN.min.js"></script>
<!-- icheck -->
<script src="BJUI/plugins/icheck/icheck.min.js"></script>
<!-- dragsort -->
<script src="BJUI/plugins/dragsort/jquery.dragsort-0.5.1.min.js"></script>
<!-- HighCharts -->
<script src="BJUI/plugins/highcharts/highcharts.js"></script>
<script src="BJUI/plugins/highcharts/highcharts-3d.js"></script>
<script src="BJUI/plugins/highcharts/themes/gray.js"></script>
<!-- ECharts -->
<script src="BJUI/plugins/echarts/echarts.js"></script>
<!-- other plugins -->
<script src="BJUI/plugins/other/jquery.autosize.js"></script>
<link href="BJUI/plugins/uploadify/css/uploadify.css" rel="stylesheet">
<script src="BJUI/plugins/uploadify/scripts/jquery.uploadify.min.js"></script>
<script src="BJUI/plugins/download/jquery.fileDownload.js"></script>

<script type="text/javascript">
	$(function(){
		//初始化插件
		BJUI.init({
	        JSPATH       : 'BJUI/',         //[可选]框架路径
	        PLUGINPATH   : 'BJUI/plugins/', //[可选]插件路径
	        loginInfo    : {url:'login_timeout.html', title:'登录', width:400, height:200}, // 会话超时后弹出登录对话框
	        statusCode   : {ok:200, error:300, timeout:301}, //[可选]
	        ajaxTimeout  : 50000, //[可选]全局Ajax请求超时时间(毫秒)
	        pageInfo     : {total:'total', pageCurrent:'pageCurrent', pageSize:'pageSize', orderField:'orderField', orderDirection:'orderDirection'}, //[可选]分页参数
	        alertMsg     : {displayPosition:'topcenter', displayMode:'slide', alertTimeout:3000}, //[可选]信息提示的显示位置，显隐方式，及[info/correct]方式时自动关闭延时(毫秒)
	        keys         : {statusCode:'statusCode', message:'message'}, //[可选]
	        ui           : {
	                         windowWidth      : 1200, //框架显示宽度，0=100%宽，> 600为则居中显示
	                         showSlidebar     : true, //[可选]左侧导航栏锁定/隐藏
	                         clientPaging     : true, //[可选]是否在客户端响应分页及排序参数
	                         overwriteHomeTab : false //[可选]当打开一个未定义id的navtab时，是否可以覆盖主navtab(我的主页)
	                       },
	        debug        : true,    // [可选]调试模式 [true|false，默认false]
	        theme        : 'sky' // 若有Cookie['bjui_theme'],优先选择Cookie['bjui_theme']。皮肤[五种皮肤:default, orange, purple, blue, red, green]
	    })

		
	});
</script>
</head>
<body>
	<form action="*">
		文本框：<input name="t" type="text" 
			data-rule="required:t" data-tip="请输入用户名" placeholder="请输入文本"
			data-ok="用户名可用" data-rule-t="/^[\d]+$/" date-e="格式错误"><br><br>
			
		<!-- data-rule="required" 效验不能为空 -->
		必填项：<input name="t1" type="text" value="" data-rule="required"><br><br>
		数值框：<input name="n1" type="number" data-toggle="spinner"><br><br>
		<textarea name="tt1" cols="30" rows="4">固定尺寸的普通多行文本框</textarea><br><br>
		<textarea cols="30" rows="1" data-toggle="autoheight">自动调整高度的多行文本框</textarea><br><br>
		日期：<input name="date" type="text" data-toggle="datepicker" readonly="readonly"><br><br>
		日期时间：<input type="text" value="2016-10-01 10:01:01" data-toggle="datepicker" data-pattern="yyyy-MM-dd HH:mm:ss" readonly="readonly"><br><br>
		复选框:
		<input type="checkbox" name="checkbox" value="1" data-toggle="icheck" data-label="复选框1">&nbsp;&nbsp;
		<input type="checkbox" name="checkbox" value="1" data-toggle="icheck" data-label="默认选择复选框2" checked="">&nbsp;&nbsp;
		<input type="checkbox" name="checkbox" value="1" data-toggle="icheck" data-label="复选框禁用" disabled=""><br><br>
		
		单选框：
		<input type="radio" name="radio1" value="1" data-toggle="icheck" data-label="单选框1">&nbsp;&nbsp;
		<input type="radio" name="radio1" value="1" data-toggle="icheck" data-label="默认选择单选框1" checked="">
		<input type="radio" name="radio1" value="1" data-toggle="icheck" data-label="禁用单选" disabled=""><br><br>
		
		单选下拉选择框：
		 <select data-toggle="selectpicker" data-width="200">
            <option value="">全部</option>
            <option value="1">下拉选项1</option>
            <option value="2" selected="">下拉选项2</option>
            <option value="3">下拉选项3</option>
            <option value="4">下拉选项4</option>
        </select><br><br>
      	  多选：
      	  <select data-toggle="selectpicker" data-width="200" multiple="">
            <option value="">全部</option>
            <option value="崇明">崇明</option>
            <option value="黄浦">黄浦</option>
            <option value="南汇">南汇</option>
            <option value="奉贤">奉贤</option>
            <option value="朱家角">朱家角</option>
        </select>	
        <!--data-refurl返回数据： [ ["崇明", "崇明"], ["黄浦", "黄浦"], ["朱家角", "朱家角"] ] -->
 		关联选择框：
 		<select name="province" data-toggle="selectpicker" data-nextselect="#j_form_city2" data-refurl="../../json/select/json_city_{value}.html">
            <option value="all">--省市--</option>
            <option value="bj">北京</option>
            <option value="sh">上海</option>
        </select>
        <select name="city" id="j_form_city2" data-toggle="selectpicker" data-nextselect="#j_form_area2" data-refurl="../../json/select/json_area_{value}.html" data-emptytxt="--城市--">
            <option value="all">--城市--</option>
        </select>
        <select name="area" id="j_form_area2" data-toggle="selectpicker" data-emptytxt="--区县--">
            <option value="all">--区县--</option>
        </select>
        
        
        <br>
        <!-- [ { "id": "01", "label": "中国", "value": "China" }, { "id": "02", "label": "日本", "value": "Jpan" }, { "id": "03", "label": "韩国", "value": "Korea" } ] -->
        <input id="inputTags" type="text" name="tags" value="" data-toggle="tags" data-url="data/tags.json" data-width="360" size="15" placeholder="输入关键字，回车提交">
   		<script>
   			$(function(){
   		     $('#inputTags').on('aftercreated.bjui.tags', function(e, data) {
   		      var value = data.value // 当前创建的标签值
   		      var item  = data.item  // 当前选定项的值(object，具体值由返回JSON决定)
   		      var tags  = data.tags  // 所有已生成标签的值，以英文逗号(,)分隔
   		      console.log(value+"|"+item+"|"+tags) ;
   		      
   		  })
   			});
   		</script>
        
        <input type="submit" value="提交">
	</form>
</body>
</html>
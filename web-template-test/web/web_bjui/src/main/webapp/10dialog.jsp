<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page isELIgnored="false" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<!-- bootstrap - css -->
<link href="BJUI/themes/css/bootstrap.css" rel="stylesheet">
<!-- core - css -->
<link href="BJUI/themes/css/style.css" rel="stylesheet">
<link href="BJUI/themes/blue/core.css" id="bjui-link-theme"
	rel="stylesheet">
<!-- plug - css -->
<link href="BJUI/plugins/kindeditor_4.1.10/themes/default/default.css"
	rel="stylesheet">
<link href="BJUI/plugins/colorpicker/css/bootstrap-colorpicker.min.css"
	rel="stylesheet">
<link href="BJUI/plugins/niceValidator/jquery.validator.css"
	rel="stylesheet">
<link href="BJUI/plugins/bootstrapSelect/bootstrap-select.css"
	rel="stylesheet">
<link href="BJUI/themes/css/FA/css/font-awesome.min.css"
	rel="stylesheet">

<!-- jquery -->
<script src="BJUI/js/jquery-1.7.2.min.js"></script>
<script src="BJUI/js/jquery.cookie.js"></script>
<!--[if lte IE 9]>
<script src="BJUI/other/jquery.iframe-transport.js"></script>    
<![endif]-->
<!-- BJUI.all 分模块压缩版 -->
<script src="BJUI/js/bjui-all.js"></script>

<!-- plugins -->
<!-- swfupload for uploadify && kindeditor -->
<script src="BJUI/plugins/swfupload/swfupload.js"></script>
<!-- kindeditor -->
<script src="BJUI/plugins/kindeditor_4.1.10/kindeditor-all.min.js"></script>
<script src="BJUI/plugins/kindeditor_4.1.10/lang/zh_CN.js"></script>
<!-- colorpicker -->
<script src="BJUI/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- ztree -->
<script src="BJUI/plugins/ztree/jquery.ztree.all-3.5.js"></script>
<!-- nice validate -->
<script src="BJUI/plugins/niceValidator/jquery.validator.js"></script>
<script src="BJUI/plugins/niceValidator/jquery.validator.themes.js"></script>
<!-- bootstrap plugins -->
<script src="BJUI/plugins/bootstrap.min.js"></script>
<script src="BJUI/plugins/bootstrapSelect/bootstrap-select.min.js"></script>
<script src="BJUI/plugins/bootstrapSelect/defaults-zh_CN.min.js"></script>
<!-- icheck -->
<script src="BJUI/plugins/icheck/icheck.min.js"></script>
<!-- dragsort -->
<script src="BJUI/plugins/dragsort/jquery.dragsort-0.5.1.min.js"></script>
<!-- HighCharts -->
<script src="BJUI/plugins/highcharts/highcharts.js"></script>
<script src="BJUI/plugins/highcharts/highcharts-3d.js"></script>
<script src="BJUI/plugins/highcharts/themes/gray.js"></script>
<!-- ECharts -->
<script src="BJUI/plugins/echarts/echarts.js"></script>
<!-- other plugins -->
<script src="BJUI/plugins/other/jquery.autosize.js"></script>
<link href="BJUI/plugins/uploadify/css/uploadify.css" rel="stylesheet">
<script src="BJUI/plugins/uploadify/scripts/jquery.uploadify.min.js"></script>
<script src="BJUI/plugins/download/jquery.fileDownload.js"></script>

<script type="text/javascript">
	$(function() {
		//初始化插件
		BJUI.init({
			JSPATH : 'BJUI/', //[可选]框架路径
			PLUGINPATH : 'BJUI/plugins/', //[可选]插件路径
			loginInfo : {
				url : 'login_timeout.html',
				title : '登录',
				width : 400,
				height : 200
			}, // 会话超时后弹出登录对话框
			statusCode : {
				ok : 200,
				error : 300,
				timeout : 301
			}, //[可选]
			ajaxTimeout : 50000, //[可选]全局Ajax请求超时时间(毫秒)
			pageInfo : {
				total : 'total',
				pageCurrent : 'pageCurrent',
				pageSize : 'pageSize',
				orderField : 'orderField',
				orderDirection : 'orderDirection'
			}, //[可选]分页参数
			alertMsg : {
				displayPosition : 'topcenter',
				displayMode : 'slide',
				alertTimeout : 3000
			}, //[可选]信息提示的显示位置，显隐方式，及[info/correct]方式时自动关闭延时(毫秒)
			keys : {
				statusCode : 'statusCode',
				message : 'message'
			}, //[可选]
			ui : {
				windowWidth : 1200, //框架显示宽度，0=100%宽，> 600为则居中显示
				showSlidebar : true, //[可选]左侧导航栏锁定/隐藏
				clientPaging : true, //[可选]是否在客户端响应分页及排序参数
				overwriteHomeTab : false
			//[可选]当打开一个未定义id的navtab时，是否可以覆盖主navtab(我的主页)
			},
			debug : true, // [可选]调试模式 [true|false，默认false]
			theme : 'sky' // 若有Cookie['bjui_theme'],优先选择Cookie['bjui_theme']。皮肤[五种皮肤:default, orange, purple, blue, red, green]
		});
		
		$(".api-test").click(function(){
			var js={name:'zhanglinfei',age:21};//被传递的参数
			$(this).dialog({id:'mydialog',
				url:'dialog/mydialog.jsp',
				title:'测试弹窗',
				width:600,
				height:500,
				data:js
				}).text('弹出框后a标签修改的内容');
			
		});
		
	});
</script>
</head>
<body>
	<h1>
		<a href="javascript:;" class="api-test">测试弹框</a>
	</h1>
	
	<br>
	<a href="dialog/mydialog.jsp" 
		data-toggle="dialog" 
		data-id="mydialog1" 
		data-title="我的业务弹窗1"
		data-data="name=zhanglinfei&age=21"
		>打开dialog</a>
		
	<br>
	<br>
	<button type="button" class="btn btn-green" 
		data-toggle="dialog" 
		data-id="mydialog2" 
		data-url="dialog/mydialog.jsp" 
		data-data="name=zhanglinfei&age=21"
		data-title="我的业务弹窗2">打开dialog</button>
	<br>
	<br>
	<button type="button" 
		class="btn btn-green" 
		data-toggle="dialog" 
		data-options="{id:'mydialog2', 
			url:'dialog/mydialog.jsp', 
			data:{name:'zhang',age:21},
			title:'我的业务弹窗2(参数集合)'}">打开dialog(参数集合)</button>
	
	<br>
	<br>
	<c:set var="name" value="zhanglinfei"/><!-- 可以直接获取本页内容 -->
	<button type="button" class="btn-green" 
		data-toggle="dialog" data-id="mydialog3" 
		data-target="#doc-dialog-target" data-title="加载容器中的内容">将本页隐藏内容显示成dialog</button></p>
	<!-- 显示的弹框内容 -->
	<div id="doc-dialog-target" data-noinit="true" class="hide">
    	<p>
    		<input type="checkbox" id="doc-dialog-checkbox" 
    			data-toggle="icheck" data-label="测试Checkbox">
    	</p>
    	<p>
    		<label>文本框：</label>
    		<input name="text" type="text" placeholder="文本框1" size="25">
    	</p>
    	<p>
    		<label>下拉框：</label>
    			<select name="box" data-toggle="selectpicker">
    			<option value="1">选项一</option>
    			<option value="2">选项二</option>
    		</select>
    	</p>${name }
	</div>
	
	<br>
	<br>
	
	<script type="text/javascript">
		function openMydialog(obj) {
   		 $(obj).dialog({id:'mydialog', url:'doc/dialog/mydialog.html', title:'我的业务弹窗'});
		}
	</script>
	<button type="button" class="btn btn-default" onclick="openMydialog(this)">js打开dialog</button>
	
	
	<br>
	<br>
	
	
	<script type="text/javascript">
    function doc_dialog_onLoad($dialog) {
        $dialog.alertmsg('info', 'onLoad回调：不填写工号是不能关闭本弹窗的。')
    }
    function doc_dialog_beforeClose($dialog) {
        var code = $dialog.find('#doc-dialog-code').val()
         
        if (code) return true
        $dialog.alertmsg('error', 'beforeClose回调：关闭弹窗前请先填入你的工号。')
      return false
    }
    function doc_dialog_onClose() {
        $(this).alertmsg('info', 'onClose回调：你刚刚关闭了一个dialog。')
    }
</script>
<button type="button" class="btn-green" 
	data-toggle="dialog" 
	data-id="mydialog4" 
	data-target="#doc-dialog-target-callback" 
	data-title="回调函数示例" 
	data-on-load="doc_dialog_onLoad" 
	data-before-close="doc_dialog_beforeClose" 
	data-on-close="doc_dialog_onClose">打开dialog</button>
	
<div id="doc-dialog-target-callback" data-noinit="true" class="hide">
    <div class="text-center">
        <h3>dialog回调函数示例</h3>
        <hr>
        <p><label>工号：</label><input type="text" name="code" id="doc-dialog-code"></p>
    </div>
</div>
	
	
</body>
</html>
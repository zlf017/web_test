<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<div class="bjui-pageFooter">
    <div class="pages">
        <span>每页</span>
        <div class="selectPagesize">
            <select data-toggle="selectpicker" data-toggle-change="changepagesize">
                <option value="5" >5</option>
                <option value="10" selected="selected">10</option>
                <option value="20" >20</option>

                <%--
                <c:forEach items="${wj_page_size_list}" var="entity" >
                    <option value="${entity.value}" <c:if test="${entity.value == pageSize}">selected="selected"</c:if>>${entity.label}</option>
                </c:forEach>
                --%>
            </select>
        </div>
        <%--LQZ 如果没有数据，显示0条--%>
        <span>条，共 <c:if test="${empty total}">0</c:if><c:if test="${not empty total}">${total}</c:if> 条</span>
    </div>
    <div class="pagination-box" data-toggle="pagination" data-total="${total}" data-page-size="${pageSize}" data-page-current="${pageCurrent}">
    </div>
</div>

<script>
    /* zxc优化开始 */

    // 解决多个列表间的字段排序冲突问题
    $(".page.unitBox.fade.in > .bjui-pageHeader > #pagerForm > [name='orderField']").val("");
    $(".page.unitBox.fade.in > .bjui-pageHeader > #pagerForm > [name='orderDirection']").val("");

    // 解决多个列表间的分页大小冲突问题
    var selectedPagesize = $(".pages > .selectPagesize > select").val();
    $(".page.unitBox.fade.in > .bjui-pageHeader > #pagerForm > [name='pageSize']").val(selectedPagesize);
    $(".page.unitBox.fade.in > .bjui-pageFooter > .pagination-box").attr('data-page-size',selectedPagesize);

    /* zxc优化结束 */
</script>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Insert title here</title>
    <!-- bootstrap - css -->
    <link href="BJUI/themes/css/bootstrap.css" rel="stylesheet">
    <!-- core - css -->
    <link href="BJUI/themes/css/style.css" rel="stylesheet">
    <link href="BJUI/themes/blue/core.css" id="bjui-link-theme"
          rel="stylesheet">
    <!-- plug - css -->
    <link href="BJUI/plugins/kindeditor_4.1.10/themes/default/default.css"
          rel="stylesheet">
    <link href="BJUI/plugins/colorpicker/css/bootstrap-colorpicker.min.css"
          rel="stylesheet">
    <link href="BJUI/plugins/niceValidator/jquery.validator.css"
          rel="stylesheet">
    <link href="BJUI/plugins/bootstrapSelect/bootstrap-select.css"
          rel="stylesheet">
    <link href="BJUI/themes/css/FA/css/font-awesome.min.css"
          rel="stylesheet">

    <!-- jquery -->
    <script src="BJUI/js/jquery-1.7.2.min.js"></script>
    <script src="BJUI/js/jquery.cookie.js"></script>
    <!--[if lte IE 9]>
    <script src="BJUI/other/jquery.iframe-transport.js"></script>
    <![endif]-->
    <!-- BJUI.all 分模块压缩版 -->
    <script src="BJUI/js/bjui-all.js"></script>

    <!-- plugins -->
    <!-- swfupload for uploadify && kindeditor -->
    <script src="BJUI/plugins/swfupload/swfupload.js"></script>
    <!-- kindeditor -->
    <script src="BJUI/plugins/kindeditor_4.1.10/kindeditor-all.min.js"></script>
    <script src="BJUI/plugins/kindeditor_4.1.10/lang/zh_CN.js"></script>
    <!-- colorpicker -->
    <script src="BJUI/plugins/colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <!-- ztree -->
    <script src="BJUI/plugins/ztree/jquery.ztree.all-3.5.js"></script>
    <!-- nice validate -->
    <script src="BJUI/plugins/niceValidator/jquery.validator.js"></script>
    <script src="BJUI/plugins/niceValidator/jquery.validator.themes.js"></script>
    <!-- bootstrap plugins -->
    <script src="BJUI/plugins/bootstrap.min.js"></script>
    <script src="BJUI/plugins/bootstrapSelect/bootstrap-select.min.js"></script>
    <script src="BJUI/plugins/bootstrapSelect/defaults-zh_CN.min.js"></script>
    <!-- icheck -->
    <script src="BJUI/plugins/icheck/icheck.min.js"></script>
    <!-- dragsort -->
    <script src="BJUI/plugins/dragsort/jquery.dragsort-0.5.1.min.js"></script>
    <!-- HighCharts -->
    <script src="BJUI/plugins/highcharts/highcharts.js"></script>
    <script src="BJUI/plugins/highcharts/highcharts-3d.js"></script>
    <script src="BJUI/plugins/highcharts/themes/gray.js"></script>
    <!-- ECharts -->
    <script src="BJUI/plugins/echarts/echarts.js"></script>
    <!-- other plugins -->
    <script src="BJUI/plugins/other/jquery.autosize.js"></script>
    <link href="BJUI/plugins/uploadify/css/uploadify.css" rel="stylesheet">
    <script src="BJUI/plugins/uploadify/scripts/jquery.uploadify.min.js"></script>
    <script src="BJUI/plugins/download/jquery.fileDownload.js"></script>

    <script type="text/javascript">
        $(function () {
            //初始化插件
            BJUI.init({
                JSPATH: 'BJUI/', //[可选]框架路径
                PLUGINPATH: 'BJUI/plugins/', //[可选]插件路径
                loginInfo: {
                    url: 'login_timeout.html',
                    title: '登录',
                    width: 400,
                    height: 200
                }, // 会话超时后弹出登录对话框
                statusCode: {
                    ok: 200,
                    error: 300,
                    timeout: 301
                }, //[可选]
                ajaxTimeout: 50000, //[可选]全局Ajax请求超时时间(毫秒)
                pageInfo: {
                    total: 'total',
                    pageCurrent: 'pageCurrent',
                    pageSize: 'pageSize',
                    orderField: 'orderField',
                    orderDirection: 'orderDirection'
                }, //[可选]分页参数
                alertMsg: {
                    displayPosition: 'topcenter',
                    displayMode: 'slide',
                    alertTimeout: 3000
                }, //[可选]信息提示的显示位置，显隐方式，及[info/correct]方式时自动关闭延时(毫秒)
                keys: {
                    statusCode: 'statusCode',
                    message: 'message'
                }, //[可选]
                ui: {
                    windowWidth: 1200, //框架显示宽度，0=100%宽，> 600为则居中显示
                    showSlidebar: true, //[可选]左侧导航栏锁定/隐藏
                    clientPaging: true, //[可选]是否在客户端响应分页及排序参数
                    overwriteHomeTab: false
                    //[可选]当打开一个未定义id的navtab时，是否可以覆盖主navtab(我的主页)
                },
                debug: true, // [可选]调试模式 [true|false，默认false]
                theme: 'sky' // 若有Cookie['bjui_theme'],优先选择Cookie['bjui_theme']。皮肤[五种皮肤:default, orange, purple, blue, red, green]
            })

        });
    </script>

</head>
<body>
<h1>配置data-nodes属性加载数树</h1>
<ul id="ztree-test-demo1" class="ztree" data-toggle="ztree"
    data-nodes="[{id:1,pid:0,name:'表单元素',faicon:'rss',children:[{id:10,pId:1,name:'按钮'},{id:11,pId:1,name:'文本框'}]}]"></ul>


<hr>
<hr>
<h1>配置data-options属性加载树</h1>
<ul id="ztree-test-demo2" class="ztree" data-toggle="ztree"
    data-options="{nodes:[{id:1,pid:0,name:'表单元素',faicon:'rss',children:[{id:10,pId:1,name:'按钮'},{id:11,pId:1,name:'文本框'}]}]}"></ul>
<hr>
<hr>
<h1>通过标签加载树</h1>
<ul id="ztree-test-demo3" class="ztree" data-toggle="ztree">
    <li data-id="1" data-pid="0" data-faicon="rss" data-faicon-close="cab">表单元素</li>
    <li data-id="10" data-pid="1" data-url="/dialog/mydialog.jsp?name=button"
        data-tabid="form-button" data-faicon="bell">按钮
    </li>
    <li data-id="11" data-pid="1" data-url="/dialog/mydialog.jsp?name=input"
        data-tabid="form-input" data-faicon="info-circle">文本框
    </li>
    <li data-id="112" data-pid="11" data-url="/dialog/mydialog.jsp?name=input1"
        data-tabid="form-input" data-faicon="info-circle">文本框
    </li>
</ul>

<hr>
<hr>
<h1>通过调用js树</h1>
<script type="text/javascript">
    function ztree_returnjson() {
        return [{
            id: 1,
            pid: 0,
            name: '表单元素',
            children: [{
                id: 10,
                pId: 1,
                name: '按钮'
            }, {
                id: 11,
                pId: 1,
                name: '文本框'
            }]
        }]
    }
</script>
<ul id="ztree-test-demo4" class="ztree" data-toggle="ztree"
    data-options="{nodes:'ztree_returnjson'}"></ul>

<br>
<h1></h1>
<ul id="treeDemo" class="ztree">
</ul>
<script type="text/javascript">
    //参数
    var setting = {
        view: {
            addHoverDom: addHoverDom, //鼠标移动到上面显示的按钮
            removeHoverDom: removeHoverDom, //隐藏自定义按钮
            selectedMulti: false //是否允许同时选中多个节点
        },
        edit: {
            enable: true,  //是否处于编辑状态
            editNameSelectAll: true,
            showRemoveBtn: showRemoveBtn,
            showRenameBtn: showRenameBtn
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        callback: {
            beforeDrag: beforeDrag,
            beforeEditName: beforeEditName,
            beforeRemove: beforeRemove,
            beforeRename: beforeRename,
            onRemove: onRemove,
            onRename: onRename
        }
    };
    //树数据
    var zNodes =[
        { id:1, pId:0, name:"父节点 1", open:true},
        { id:11, pId:1, name:"叶子节点 1-1"},
        { id:12, pId:1, name:"叶子节点 1-2"},
        { id:13, pId:1, name:"叶子节点 1-3"},
        { id:2, pId:0, name:"父节点 2", open:false},
        { id:21, pId:2, name:"叶子节点 2-1"},
        { id:22, pId:2, name:"叶子节点 2-2"},
        { id:23, pId:2, name:"叶子节点 2-3"},
        { id:3, pId:0, name:"父节点 3", open:true},
        { id:31, pId:3, name:"叶子节点 3-1"},
        { id:32, pId:3, name:"叶子节点 3-2"},
        { id:33, pId:3, name:"叶子节点 3-3"}
    ];
    var log, className = "dark";

    function beforeDrag(treeId, treeNodes) {
        return false;
    }
    //进入编辑状态
    function beforeEditName(treeId, treeNode) {
        className = (className === "dark" ? "":"dark");
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        zTree.selectNode(treeNode);
        setTimeout(function() {
            if (confirm("进入节点 -- " + treeNode.name + " 的编辑状态吗？")) {
                setTimeout(function() {
                    zTree.editName(treeNode);
                }, 0);
            }
        }, 0);
        return false;
    }
    function beforeRemove(treeId, treeNode) {
        className = (className === "dark" ? "":"dark");
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        zTree.selectNode(treeNode);
        //返回TRUE删除节点
        return confirm("确认删除 节点 -- " + treeNode.name + " 吗？");
    }
    //删除成功后回调方法
    function onRemove(e, treeId, treeNode) {
        console.log(treeNode.name);
        //showLog("[ "+getTime()+" onRemove ]&nbsp;&nbsp;&nbsp;&nbsp; " + treeNode.name);
    }
    function beforeRename(treeId, treeNode, newName, isCancel) {
        console.log(treeNode);
        className = (className === "dark" ? "":"dark");
        if (newName.length == 0) {
            setTimeout(function() {
                var zTree = $.fn.zTree.getZTreeObj("treeDemo");
                zTree.cancelEditName();
                alert("节点名称不能为空.");
            }, 0);
            return false;
        }
        return true;
    }
    function onRename(e, treeId, treeNode, isCancel) {
    }
    function showRemoveBtn(treeId, treeNode) {
//        return !treeNode.isFirstNode;
        //是否显示删除按钮
        return true;
    }
    function showRenameBtn(treeId, treeNode) {
//        return !treeNode.isLastNode;
        //是否显示修改按钮
        return true;
    }

    function getTime() {
        var now= new Date(),
                h=now.getHours(),
                m=now.getMinutes(),
                s=now.getSeconds(),
                ms=now.getMilliseconds();
        return (h+":"+m+":"+s+ " " +ms);
    }

    var newCount = 1;

    function addHoverDom(treeId, treeNode) {
        var sObj = $("#" + treeNode.tId + "_span");
        if (treeNode.editNameFlag || $("#addBtn_"+treeNode.tId).length>0) return;
        var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
                + "' title='add node' onfocus='this.blur();'></span>";
        sObj.after(addStr);
        var btn = $("#addBtn_"+treeNode.tId);
        if (btn) btn.bind("click", function(){
            var zTree = $.fn.zTree.getZTreeObj("treeDemo");
            zTree.addNodes(treeNode, {id:(100 + newCount), pId:treeNode.id, name:"new node" + (newCount++)});
            return false;
        });
    };
    function removeHoverDom(treeId, treeNode) {
        $("#addBtn_"+treeNode.tId).unbind().remove();
    };


    function selectAll() {
        var zTree = $.fn.zTree.getZTreeObj("treeDemo");
        zTree.setting.edit.editNameSelectAll =  $("#selectAll").attr("checked");
    }

    $(document).ready(function(){
        $.fn.zTree.init($("#treeDemo"), setting, zNodes);
       // $("#selectAll").bind("click", selectAll);
    });
</script>
</body>
</html>
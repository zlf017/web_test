package com.zlf.web_bjui.servlet.util;

import com.zlf.web_bjui.servlet.vo.MenuVo;

/**
 * <p>Project: bee-projects</p>
 * <p>Title: EduCourseNoteBean.java</p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2016 </p>
 * <p>Company: 华炜云商科技有限公司 www.hwtech.cc</p>
 *
 * @author Administrator
 * @version 1.0
 * @date 2017/1/18 14:01
 */
public class MenuUtil {

    public static MenuVo menuVoRoot;



    public static void createMeuRoot(){
        menuVoRoot=new MenuVo("系统");
        MenuVo menuVo1=new MenuVo("参数设置");

        MenuVo menuVo11=new MenuVo("邮箱配置");
        menuVo1.addMenu(menuVo11);
        MenuVo menuVo12=new MenuVo("短信配置");
        menuVo1.addMenu(menuVo12);

        MenuVo menuVo2=new MenuVo("邮箱发送");
        MenuVo menuVo21=new MenuVo("邮箱发送记录");
        menuVo2.addMenu(menuVo21);
        MenuVo menuVo22=new MenuVo("发送邮箱");
        menuVo2.addMenu(menuVo22);

        MenuVo menuVo3=new MenuVo("短信发送");
        MenuVo menuVo31=new MenuVo("短信发送记录");
        menuVo3.addMenu(menuVo31);
        MenuVo menuVo32=new MenuVo("发送短信");
        menuVo3.addMenu(menuVo32);

        menuVoRoot.addMenu(menuVo1);
        menuVoRoot.addMenu(menuVo2);
        menuVoRoot.addMenu(menuVo3);
    }

}

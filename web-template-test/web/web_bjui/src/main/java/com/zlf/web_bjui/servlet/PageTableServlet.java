package com.zlf.web_bjui.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zlf.web_bjui.servlet.util.JDBCUtil;

public class PageTableServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		Map<String, String[]> map = req.getParameterMap();
		System.out.println(map);
		int pageSize = Integer.parseInt(req.getParameter("pageSize")==null?"5":req.getParameter("pageSize"));// 页面大小
		int currentNum = Integer.parseInt(req.getParameter("pageCurrent")==null?"1":req.getParameter("pageCurrent"));// 当前页数
		String totalStr = req.getParameter("total");
		JDBCUtil jdbc = new JDBCUtil();
		try {
			jdbc.getConnection();
			if (totalStr == null || "".equals(totalStr)) {
				String sql="select count(*) as c from page_test";
				List<Map<String,Object>> list=jdbc.executeQury(sql);
				totalStr=String.valueOf(list.get(0).get("c"));
			}
			int total = Integer.parseInt(totalStr);
			
			int startNum = 0;
			if (currentNum > 1) {
				startNum = (currentNum - 1) * pageSize;
			}

			String sql = "select id,name,age,log from page_test limit "+startNum+","+pageSize;
			System.out.println(sql);
			req.setAttribute("list", jdbc.executeQury(sql));
			req.setAttribute("total", total);
			req.setAttribute("pageCurrent", currentNum);
			req.setAttribute("pageSize", pageSize);
			req.getRequestDispatcher("08page.jsp").forward(req, resp);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			jdbc.close();
		}

	}

}

package com.zlf.web_bjui.servlet.entity;

import java.util.Date;

public class Person {
	private String name;
	private int age;
	private Date date;
	private String oper="<a href=‘form.html?id=1‘ class=‘btn btn-green‘ data-toggle=‘navtab‘ data-id=‘form‘ data-reload-warn=‘本页已有打开的内容，确定将刷新本页内容，是否继续？‘ data-title=‘编辑-孙悟空‘>编辑</a>";
	
	
	
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Person(String name, int age, Date date) {
		super();
		this.name = name;
		this.age = age;
		this.date = date;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getOper() {
		return oper;
	}
	public void setOper(String oper) {
		this.oper = oper;
	}
	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", date=" + date + "]";
	}
	
}

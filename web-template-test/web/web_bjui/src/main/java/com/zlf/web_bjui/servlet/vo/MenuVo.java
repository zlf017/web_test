package com.zlf.web_bjui.servlet.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Project: bee-projects</p>
 * <p>Title: EduCourseNoteBean.java</p>
 * <p>Description:   </p>
 * <p>Copyright: Copyright (c) 2016 </p>
 * <p>Company: 华炜云商科技有限公司 www.hwtech.cc</p>
 *
 * @author Administrator
 * @version 1.0
 * @date 2017/1/18 13:54
 */
public class MenuVo {

    private String name;
    private String url;
    private String id;
    private List<MenuVo> menuVoList =new ArrayList<>();

    public void addMenu(MenuVo menuVo){
        menuVoList.add(menuVo);
    }

    public MenuVo(){}

    public MenuVo(String name) {
        this.name = name;
    }

    public MenuVo(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public List<MenuVo> getMenuVoList() {
        return menuVoList;
    }

    public void setMenuVoList(List<MenuVo> menuVoList) {
        this.menuVoList = menuVoList;
    }
}

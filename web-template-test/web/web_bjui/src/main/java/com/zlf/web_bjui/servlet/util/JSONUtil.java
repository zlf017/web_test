package com.zlf.web_bjui.servlet.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

/**
 * json数据转换
 * @author zlf
 *
 */
public class JSONUtil implements JsonValueProcessor {
	public static final String DATE_FORMAT_DEFAULT="yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_DATA="yyyy-MM-dd";
	public static final String DATE_FORMAT_TIME="HH:mm:ss";
	private DateFormat dateFormat;

	/**
	 * 构造方法.
	 * 
	 * @param datePattern
	 *            日期格式
	 */
	public JSONUtil(String datePattern) {
		try {
			dateFormat = new SimpleDateFormat(datePattern);
		} catch (Exception ex) {
			dateFormat = new SimpleDateFormat(DATE_FORMAT_DEFAULT);
		}
	}

	public Object processArrayValue(Object value, JsonConfig jsonConfig) {
		return process(value);
	}

	public Object processObjectValue(String key, Object value, JsonConfig jsonConfig) {
		return process(value);
	}

	private Object process(Object value) {
		if (value == null) {
			value = new Date(); // 为null时返回当前日期，也可以返回"",看需要
		}
		return dateFormat.format((Date) value);
	}

	public String map2JSonStr(Map map) {
		JsonConfig jsonConfig = new JsonConfig();
		JSONUtil beanProcessor = new JSONUtil("yyyy-MM-dd HH:mm:ss");
		jsonConfig.registerJsonValueProcessor(Date.class, beanProcessor);

		JSONObject jsonObject = JSONObject.fromObject(map, jsonConfig);

		return jsonObject.toString();
	}
	/**
	 * 将对象转换成json对象
	 * @param obj
	 * @return
	 */
	// 将对象转换为json string，使用上面定义的的日期格式
	public static JSONObject obj2JsonObj(Object obj,String date_format) {
		JsonConfig jsonConfig = new JsonConfig();
		JSONUtil beanProcessor = new JSONUtil(date_format);
		jsonConfig.registerJsonValueProcessor(Date.class, beanProcessor);

		JSONObject jsonObject = JSONObject.fromObject(obj, jsonConfig);

		return jsonObject;
	}
	/**
	 * 将数组集合转换成jsonarray
	 * @param coll
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static JSONArray arrayToJson(List coll){
		JsonConfig jsonConfig = new JsonConfig();
		JSONUtil beanProcessor = new JSONUtil("yyyy-MM-dd HH:mm:ss");
		jsonConfig.registerJsonValueProcessor(Date.class, beanProcessor);
		return JSONArray.fromObject(coll, jsonConfig);
	}
	
}
package com.zlf.web_bjui.servlet.entity;

public class Page {
	private int page;//页数
	private int pageSize=5;
	private int pageSum;//总页数
	private int currentnum;//当前页数
	private int sumnum;//总数量
	public int getPage() {
		return page;
	}
	public void setPage(int page) {
		this.page = page;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getPageSum() {
		return pageSum;
	}
	public void setPageSum(int pageSum) {
		this.pageSum = pageSum;
	}
	public int getCurrentnum() {
		return currentnum;
	}
	public void setCurrentnum(int currentnum) {
		this.currentnum = currentnum;
	}
	public int getSumnum() {
		return sumnum;
	}
	public void setSumnum(int sumnum) {
		this.sumnum = sumnum;
	}
	
	
}

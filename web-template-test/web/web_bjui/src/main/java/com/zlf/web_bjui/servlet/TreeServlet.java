package com.zlf.web_bjui.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.zlf.web_bjui.servlet.util.JDBCUtil;

import net.sf.json.JSONArray;

public class TreeServlet extends HttpServlet{

	private static final long serialVersionUID = -4104045436911767301L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		int tid=0;
		try{
			tid = Integer.valueOf(req.getParameter("id"));
			
			System.out.println("tid:"+tid);
		}catch(Exception e){
			
		}
		Map<String, String[]> map2 = req.getParameterMap();
		System.out.println("map:"+map2);
		JDBCUtil jdbc=new JDBCUtil();
		List<Map<String, Object>> list=new ArrayList<>();
		resp.setCharacterEncoding("utf-8");
		PrintWriter writer = resp.getWriter();
		
		try {
//			list = jdbc.execute("SELECT id,text,tid,state,data FROM think_nav where tid="+tid);
			Statement statement = jdbc.getConnection().createStatement();
			ResultSet resultSet = statement.executeQuery(
					"SELECT id as cid,text,tid as b,state,data FROM think_nav  where tid="+tid);
			
			while(resultSet.next()){
				
				Map<String,Object> map=new HashMap<>();
				map.put("id", resultSet.getInt("cid"));
				map.put("text", resultSet.getString("text"));
				map.put("tid", resultSet.getString("b"));
				map.put("state", resultSet.getString("state"));
				map.put("data", resultSet.getString("data"));
				list.add(map);
			}
			
			writer.print(JSONArray.fromObject(list).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			jdbc.close();
			writer.close();
		}
	}
	
}

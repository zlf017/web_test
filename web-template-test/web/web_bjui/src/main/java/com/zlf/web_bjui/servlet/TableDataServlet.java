package com.zlf.web_bjui.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.zlf.web_bjui.servlet.entity.Person;
import com.zlf.web_bjui.servlet.util.JSONUtil;

public class TableDataServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		resp.setCharacterEncoding("utf-8");
		Person p1=new Person("asas", 12, new Date());
		Person p2=new Person("asas", 12, new Date());
		Person p3=new Person("asas", 12, new Date());
		Person p4=new Person("asas", 12, new Date());
		Person p5=new Person("asas", 12, new Date());
		
		List<Person> list=new ArrayList<>();
		list.add(p1);
		list.add(p2);
		list.add(p3);
		list.add(p4);
		list.add(p5);
		String str=JSONUtil.arrayToJson(list).toString();
		System.out.println(str);
		PrintWriter writer = resp.getWriter();
		writer.println(str);
		writer.close();
	}
	
}

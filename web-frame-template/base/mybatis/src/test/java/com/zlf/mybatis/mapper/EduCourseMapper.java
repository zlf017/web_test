package com.zlf.mybatis.mapper;

import com.zlf.mybatis.dialect.Page;
import com.zlf.mybatis.entity.EduCourse;

/**
 * Created by Administrator on 2017/1/2.
 *
 * @author zlf
 */
public interface EduCourseMapper {

    public Page<EduCourse> queryAll();


}

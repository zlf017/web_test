package com.zlf.mybatis.test;

import com.zlf.mybatis.entity.EduCourse;
import com.zlf.mybatis.util.MyBatis;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

import java.util.List;

/**
 * Created by Administrator on 2017/1/2.
 *
 * @author zlf
 */
public class TestMybatis {




    @Test
    public void testQueryAll() {
       SqlSession session= MyBatis.getSession();
        List<EduCourse> list=session.selectList("com.zlf.mybatis.mapper.EduCourseMapper.queryAll");
       for(EduCourse eduCourse:list){
           System.out.println(eduCourse);
       }
        session.close();
    }
    @Test
    public void testQueryPage(){
        SqlSession session= MyBatis.getSession();
        List<EduCourse> list=session.selectList("com.zlf.mybatis.mapper.EduCourseMapper.queryAll");
        for(EduCourse eduCourse:list){
            System.out.println(eduCourse);
        }
        session.close();
    }

}

package com.zlf.mybatis.entity;

import java.math.BigDecimal;
import java.util.Date;

public class EduCourse {
    private Integer id;

    private String name;

    private String vtIds;

    private Integer esId;

    private String esLink;

    private Date liveStartDt;

    private Date liveEndDt;

    private Integer salesType;

    private BigDecimal orgPrice;

    private BigDecimal salesPrice;

    private String summary;

    private String content;

    private String coverPcImg;

    private String coverAppImg;

    private String coverPackageImg;

    private Integer totalClassHours;

    private String logoImg;

    private Integer salesMode;

    private Date byExpDt;

    private Integer byDaysCnt;

    private Integer salesInitQty;

    private Integer salesQty;

    private Integer visitInitQty;

    private Integer visitQty;

    private BigDecimal totalSalesAmt;

    private String couponId;

    private Integer epId;

    private Integer sort;

    private Integer status;

    private Date createdDt;

    private Date updatedDt;

    private Integer creator;

    private Integer updator;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVtIds() {
        return vtIds;
    }

    public void setVtIds(String vtIds) {
        this.vtIds = vtIds;
    }

    public Integer getEsId() {
        return esId;
    }

    public void setEsId(Integer esId) {
        this.esId = esId;
    }

    public String getEsLink() {
        return esLink;
    }

    public void setEsLink(String esLink) {
        this.esLink = esLink;
    }

    public Date getLiveStartDt() {
        return liveStartDt;
    }

    public void setLiveStartDt(Date liveStartDt) {
        this.liveStartDt = liveStartDt;
    }

    public Date getLiveEndDt() {
        return liveEndDt;
    }

    public void setLiveEndDt(Date liveEndDt) {
        this.liveEndDt = liveEndDt;
    }

    public Integer getSalesType() {
        return salesType;
    }

    public void setSalesType(Integer salesType) {
        this.salesType = salesType;
    }

    public BigDecimal getOrgPrice() {
        return orgPrice;
    }

    public void setOrgPrice(BigDecimal orgPrice) {
        this.orgPrice = orgPrice;
    }

    public BigDecimal getSalesPrice() {
        return salesPrice;
    }

    public void setSalesPrice(BigDecimal salesPrice) {
        this.salesPrice = salesPrice;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCoverPcImg() {
        return coverPcImg;
    }

    public void setCoverPcImg(String coverPcImg) {
        this.coverPcImg = coverPcImg;
    }

    public String getCoverAppImg() {
        return coverAppImg;
    }

    public void setCoverAppImg(String coverAppImg) {
        this.coverAppImg = coverAppImg;
    }

    public String getCoverPackageImg() {
        return coverPackageImg;
    }

    public void setCoverPackageImg(String coverPackageImg) {
        this.coverPackageImg = coverPackageImg;
    }

    public Integer getTotalClassHours() {
        return totalClassHours;
    }

    public void setTotalClassHours(Integer totalClassHours) {
        this.totalClassHours = totalClassHours;
    }

    public String getLogoImg() {
        return logoImg;
    }

    public void setLogoImg(String logoImg) {
        this.logoImg = logoImg;
    }

    public Integer getSalesMode() {
        return salesMode;
    }

    public void setSalesMode(Integer salesMode) {
        this.salesMode = salesMode;
    }

    public Date getByExpDt() {
        return byExpDt;
    }

    public void setByExpDt(Date byExpDt) {
        this.byExpDt = byExpDt;
    }

    public Integer getByDaysCnt() {
        return byDaysCnt;
    }

    public void setByDaysCnt(Integer byDaysCnt) {
        this.byDaysCnt = byDaysCnt;
    }

    public Integer getSalesInitQty() {
        return salesInitQty;
    }

    public void setSalesInitQty(Integer salesInitQty) {
        this.salesInitQty = salesInitQty;
    }

    public Integer getSalesQty() {
        return salesQty;
    }

    public void setSalesQty(Integer salesQty) {
        this.salesQty = salesQty;
    }

    public Integer getVisitInitQty() {
        return visitInitQty;
    }

    public void setVisitInitQty(Integer visitInitQty) {
        this.visitInitQty = visitInitQty;
    }

    public Integer getVisitQty() {
        return visitQty;
    }

    public void setVisitQty(Integer visitQty) {
        this.visitQty = visitQty;
    }

    public BigDecimal getTotalSalesAmt() {
        return totalSalesAmt;
    }

    public void setTotalSalesAmt(BigDecimal totalSalesAmt) {
        this.totalSalesAmt = totalSalesAmt;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Integer getEpId() {
        return epId;
    }

    public void setEpId(Integer epId) {
        this.epId = epId;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getCreatedDt() {
        return createdDt;
    }

    public void setCreatedDt(Date createdDt) {
        this.createdDt = createdDt;
    }

    public Date getUpdatedDt() {
        return updatedDt;
    }

    public void setUpdatedDt(Date updatedDt) {
        this.updatedDt = updatedDt;
    }

    public Integer getCreator() {
        return creator;
    }

    public void setCreator(Integer creator) {
        this.creator = creator;
    }

    public Integer getUpdator() {
        return updator;
    }

    public void setUpdator(Integer updator) {
        this.updator = updator;
    }

    @Override
    public String toString() {
        return "EduCourse{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", vtIds='" + vtIds + '\'' +
                ", esId=" + esId +
                ", esLink='" + esLink + '\'' +
                ", liveStartDt=" + liveStartDt +
                ", liveEndDt=" + liveEndDt +
                ", salesType=" + salesType +
                ", orgPrice=" + orgPrice +
                ", salesPrice=" + salesPrice +
                ", summary='" + summary + '\'' +
                ", content='" + content + '\'' +
                ", coverPcImg='" + coverPcImg + '\'' +
                ", coverAppImg='" + coverAppImg + '\'' +
                ", coverPackageImg='" + coverPackageImg + '\'' +
                ", totalClassHours=" + totalClassHours +
                ", logoImg='" + logoImg + '\'' +
                ", salesMode=" + salesMode +
                ", byExpDt=" + byExpDt +
                ", byDaysCnt=" + byDaysCnt +
                ", salesInitQty=" + salesInitQty +
                ", salesQty=" + salesQty +
                ", visitInitQty=" + visitInitQty +
                ", visitQty=" + visitQty +
                ", totalSalesAmt=" + totalSalesAmt +
                ", couponId='" + couponId + '\'' +
                ", epId=" + epId +
                ", sort=" + sort +
                ", status=" + status +
                ", createdDt=" + createdDt +
                ", updatedDt=" + updatedDt +
                ", creator=" + creator +
                ", updator=" + updator +
                '}';
    }
}
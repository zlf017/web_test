package com.zlf.mybatis.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import javax.security.auth.login.Configuration;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Administrator on 2017/1/2.
 *
 * @author zlf
 */
public class MyBatis {
    private static  SqlSessionFactory sqlSessionFactory;
    private static void initialFactory() {
        String resource = "mybatis.xml";
        try {
            InputStream in = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(in);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static SqlSession getSession() {
        if(sqlSessionFactory == null) {
            initialFactory();
        }
        return sqlSessionFactory.openSession();
    }

}
